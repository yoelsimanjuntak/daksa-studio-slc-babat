<?php
$arrMedia = array();
if(!empty($data[COL_QUESTIMAGE])) {
  $files_ = explode(",", $data[COL_QUESTIMAGE]);
  foreach($files_ as $f) {
    $fPath = MY_UPLOADPATH.$f;
    if(file_exists($fPath)) {
      $fType = pathinfo($fPath, PATHINFO_EXTENSION);
      $fData = file_get_contents($fPath);
      $fBase64 = 'data:image/'. $fType.';base64,'. base64_encode($fData);

      $arrMedia[] = array(
        'Uniq'=>$f,
        'ImgURL'=>MY_UPLOADURL.$f,
        'ImgPath'=>$fBase64
      );
    }
  }
}
$arrOpt = array();
if(empty($data) && !empty($rmodel) && !empty($rmodel[COL_TESTOPTION])) {
  $arrOpt = json_decode($rmodel[COL_TESTOPTION]);
} else if(!empty($data) && !empty($data[COL_QUESTOPTION])) {
  $arrOpt = json_decode($data[COL_QUESTOPTION]);
}
?>
<style>
.contenteditable {
  width: 1% !important;
  flex: 1 1 auto !important;
  padding: 0.375rem 0.75rem;
  border-top: 1px solid #dedede;
  border-bottom: 1px solid #dedede;
}
.contenteditable p {
  margin: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/master/test')?>">Model</a></li>
          <li class="breadcrumb-item"><a href="<?=$prevUrl?>"><?=$prevText?></a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <form id="form-main" action="<?=current_url()?>" method="post">
            <div class="card-header">
              <a href="<?=$prevUrl?>" class="btn btn-sm btn-outline-secondary"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>
              <button type="submit" class="btn btn-sm btn-outline-success">SIMPAN&nbsp;<i class="far fa-arrow-circle-right"></i></button>
            </div>
            <div class="card-body">
              <?php
              if(!empty($data)) {
                ?>
                <input type="hidden" name="IDTest" value="<?=$IDTest?>" />
                <div class="row mb-3 pb-2 div-quest">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Pertanyaan</label>
                      <textarea class="ckeditor" rows="5" name="<?=COL_QUESTTEXT?>"><?=$data[COL_QUESTTEXT]?></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Gambar&nbsp;<small class="text-muted">(<strong>Catatan:</strong> media yang diunggah berupa gambar / foto dengan ukuran maks. 5MB.)</small></label>
                      <input type="hidden" name="<?=COL_QUESTIMAGE?>" value='<?=json_encode($arrMedia)?>' />
                      <div class="row">
                        <div class="col-sm-12">
                          <!--<p>
                            <button type="button" id="btn-add-media" class="btn btn-primary btn-sm"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
                          </p>-->
                          <div class="col-sm-12 p-2" style="border: 1px solid #dedede; border-radius: .25rem">
                            <div class="div-preview row p-2">
                              <div class="d-none preview-blueprint ml-2" style="width: 150px;height: 100px; border: 1px solid #dedede; flex-direction: column">
                                <div class="row m-1">
                                  <div class="col-sm-12 p-0 div-info">
                                  </div>
                                </div>
                                <div class="row m-1 mt-auto">
                                  <div class="col-sm-12 p-0">
                                    <button type="button" class="btn btn-xs btn-danger btn-block btn-del-media"><i class="far fa-times-circle"></i></button>
                                  </div>
                                </div>
                              </div>
                              <button type="button" class="btn btn-app btn-lg m-0 ml-2 btn-add-media" style="height: 100px; font-size: 14px">
                                <i class="fas fa-plus-circle"></i>TAMBAH
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Tipe Pertanyaan</label>
                          <select name="<?=COL_QUESTTYPE?>" class="form-control" style="width: 100% !important" required>
                            <option value="MUL" <?=!empty($data)&&$data[COL_QUESTTYPE]=='MUL'?'selected':''?>>PILIHAN GANDA</option>
                            <option value="TEXT" <?=!empty($data)&&$data[COL_QUESTTYPE]=='TEXT'?'selected':''?>>ISIAN</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label>Kelompok <small>(OPSIONAL)</small></label>
                          <div class="input-group mb-3">
                            <select name="<?=COL_QUESTGROUP?>" class="form-control select-group no-select2" data-selected="<?=!empty($data)&&!empty($data[COL_QUESTGROUP])?$data[COL_QUESTGROUP]:''?>">
                              <option value="">--</option>
                            </select>
                            <div class="input-group-append">
                              <a href="<?=site_url('site/master/question-group-add/'.$IDTest)?>" class="btn btn-primary btn-add-group"><i class="far fa-plus-circle"></i></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 div-key">
                    <div class="form-group row">
                      <div class="col-sm-6">
                        <label>Kunci Jawaban</label>
                        <input type="text" class="form-control" name="QuestKey" value="<?=!empty($arrOpt)&&count($arrOpt)>0?$arrOpt[0]->Opt:''?>" />
                      </div>
                      <div class="col-sm-6">
                        <label>Bobot</label>
                        <input type="text" class="form-control uang text-right" name="QuestWeight"  value="<?=!empty($arrOpt)&&count($arrOpt)>0?$arrOpt[0]->Val:''?>" />
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 div-option">
                    <label>Opsi & Bobot</label>
                    <div class="row">
                      <?php
                      foreach($arrOpt as $r) {
                        ?>
                        <div class="col-sm-6">
                          <div class="input-group mb-3">
                            <div class="input-group-prepend">
                              <span class="input-group-text font-weight-bold"><?=$r->Opt?></span>
                            </div>
                            <input type="hidden" name="<?='OPT[]'?>" value="<?=$r->Opt?>">
                            <input type="hidden" name="<?='VAL__'.$r->Opt?>" value="<?=$r->Val?>">
                            <input type="hidden" name="<?='TXT__'.$r->Opt?>" value="<?=$r->Txt?>">
                            <div id="<?='TXT__'.$r->Opt?>" class="contenteditable" contenteditable="true">
                              <?=$r->Txt?>
                            </div>
                            <div class="input-group-append">
                              <span class="input-group-text text-sm">
                                BOBOT =<a href="" class="font-weight-bold btn-changeval pl-2" style="text-decoration-line: underline !important; text-decoration-style: dotted !important"><?=$r->Val?></a>
                              </span>
                            </div>
                          </div>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <?php
              } else {
                $num = 1;
                if(!empty($_GET['num'])) $num = $_GET['num'];
                for($i=0; $i<$num; $i++) {
                  ?>
                  <input type="hidden" name="IDTest" value="<?=$IDTest?>" />
                  <div class="row mb-3 pb-2 div-quest" <?=$num>1&&$i!=$num-1?'style="border-bottom: 2px solid #dedede; border-bottom-style: dotted"':''?>>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Pertanyaan</label>
                        <textarea class="ckeditor" rows="5" name="<?=COL_QUESTTEXT.'['.$i.']'?>"><?=!empty($rmodel) && !empty($rmodel[COL_TESTDEFAULT])?$rmodel[COL_TESTDEFAULT]:''?></textarea>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Gambar&nbsp;<small class="text-muted">(<strong>Catatan:</strong> media yang diunggah berupa gambar / foto dengan ukuran maks. 5MB.)</small></label>
                        <input type="hidden" name="<?=COL_QUESTIMAGE.'['.$i.']'?>" />
                        <div class="row">
                          <div class="col-sm-12">
                            <!--<p>
                              <button type="button" id="btn-add-media" class="btn btn-primary btn-sm"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
                            </p>-->
                            <div class="col-sm-12 p-2" style="border: 1px solid #dedede; border-radius: .25rem">
                              <div class="div-preview row p-2">
                                <div class="d-none preview-blueprint ml-2" style="width: 150px;height: 100px; border: 1px solid #dedede; flex-direction: column">
                                  <div class="row m-1">
                                    <div class="col-sm-12 p-0 div-info">
                                    </div>
                                  </div>
                                  <div class="row m-1 mt-auto">
                                    <div class="col-sm-12 p-0">
                                      <button type="button" class="btn btn-xs btn-danger btn-block btn-del-media"><i class="far fa-times-circle"></i></button>
                                    </div>
                                  </div>
                                </div>
                                <button type="button" class="btn btn-app btn-lg m-0 ml-2 btn-add-media" style="height: 100px; font-size: 14px">
                                  <i class="fas fa-plus-circle"></i>TAMBAH
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Tipe Pertanyaan</label>
                            <select name="<?=COL_QUESTTYPE.'['.$i.']'?>" class="form-control" style="width: 100% !important" required>
                              <option value="MUL" <?=!empty($data)&&$data[COL_QUESTTYPE]=='MUL'?'selected':''?>>PILIHAN GANDA</option>
                              <option value="TEXT" <?=!empty($data)&&$data[COL_QUESTTYPE]=='TEXT'?'selected':''?>>ISIAN</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label>Kelompok <small>(OPSIONAL)</small></label>
                            <div class="input-group mb-3">
                              <select name="<?=COL_QUESTGROUP.'['.$i.']'?>" class="form-control select-group no-select2">
                                <option value="">--</option>
                              </select>
                              <div class="input-group-append">
                                <a href="<?=site_url('site/master/question-add-group/'.$IDTest)?>" class="btn btn-primary btn-add-group"><i class="far fa-plus-circle"></i></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6 div-key">
                      <div class="form-group row">
                        <div class="col-sm-6">
                          <label>Kunci Jawaban</label>
                          <input type="text" class="form-control" name="QuestKey[<?=$i?>]" />
                        </div>
                        <div class="col-sm-6">
                          <label>Bobot</label>
                          <input type="text" class="form-control uang text-right" name="QuestWeight[<?=$i?>]" />
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12 div-option">
                      <label>Opsi & Bobot</label>
                      <div class="row">
                        <?php
                        foreach($arrOpt as $r) {
                          ?>
                          <div class="col-sm-6">
                            <div class="input-group mb-3">
                              <div class="input-group-prepend">
                                <span class="input-group-text font-weight-bold"><?=$r->Opt?></span>
                              </div>
                              <input type="hidden" name="<?='OPT'.'['.$i.'][]'?>" value="<?=$r->Opt?>">
                              <input type="hidden" name="<?='VAL__'.$r->Opt.'['.$i.']'?>" value="0">
                              <input type="hidden" name="<?='TXT__'.$r->Opt.'['.$i.']'?>" value="<?=!empty($r->Def)?$r->Def:''?>">
                              <div id="<?='TXT__'.$r->Opt.'['.$i.']'?>" class="contenteditable" contenteditable="true">
                                <?=!empty($r->Def)?$r->Def:''?>
                              </div>
                              <div class="input-group-append">
                                <span class="input-group-text text-sm">
                                  BOBOT =<a href="" class="font-weight-bold btn-changeval pl-2" style="text-decoration-line: underline !important; text-decoration-style: dotted !important">0</a>
                                </span>
                              </div>
                            </div>
                          </div>
                          <?php
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                  <?php
                }
              }
              ?>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modalMedia" tabindex="-1" role="dialog" data-target="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">UNGGAH MEDIA</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>FILE</label>
          <div class="custom-file">
            <input name="FileMedia" class="custom-file-input" type="file" onchange="previewFile()" accept="image/*">
            <label class="custom-file-label" for="file">PILIH FILE</label>
          </div>
        </div>
        <p>
          <img class="preview text-center" src="" style="max-width: 100%; display: none" alt="Preview">
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
        <button type="button" class="btn btn-primary" id="btn-add-media"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
      </div>
    </div>
  </div>
</div>
<script>
function previewFile() {
  const preview = $('.preview');
  const file = $("[name=FileMedia]").prop('files')[0];
  const reader = new FileReader();

  reader.addEventListener("load", function () {
    preview.attr('src', reader.result);
    preview.show();
  }, false);

  if (file) {
    reader.readAsDataURL(file);
  }
}

function addPostImages(obj, target) {
  var arrPostImages = $('[name="'+target+'"]').val();
  if(arrPostImages) {
    arrPostImages = JSON.parse(arrPostImages);
  } else {
    arrPostImages = [];
  }
  arrPostImages.push(obj);
  console.log('arr', arrPostImages);
  $('[name="'+target+'"]').val(JSON.stringify(arrPostImages)).trigger('change');
}

$(document).ready(function() {
  var form = $('#form-main');
  CKEDITOR.config.toolbarGroups = [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'align', 'bidi', 'blocks', 'paragraph' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'links', groups: [ 'links' ] },
		'/',
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];
  CKEDITOR.config.removeButtons = 'Source,Save,Templates,NewPage,ExportPdf,Preview,Print,Find,Replace,SelectAll,Scayt,CopyFormatting,RemoveFormat,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Link,Unlink,Anchor,Image,Smiley,PageBreak,Iframe,TextColor,BGColor,Maximize,ShowBlocks,About';
  CKEDITOR.config.height = 150;

  bsCustomFileInput.init();

  $('.select-group').load('<?=site_url('site/master/question-group-load/'.$IDTest)?>', function(){
    var sel = $(this).data('selected');
    $(this).select2({
      width: 'resolve',
      theme: 'bootstrap4',
      minimumResultsForSearch: -1
    });

    if(sel) {
      $(this).val(sel).trigger('change');
    }
  });
  $('.btn-add-group').click(function(){
    var url = $(this).attr('href');
    var dis = $(this);
    swal({
      closeOnClickOutside: true,
      buttons: ['BATAL','SUBMIT'],
      text: 'TAMBAH KELOMPOK',
      content: {
        element: "input",
        attributes: {
          placeholder: 'KELOMPOK',
          type: "text"
        }
      },
    }).then(function(val){
      if(val) {
        $('.select-group').load('<?=site_url('site/master/question-group-load/'.$IDTest)?>', {Group: val}, function(){
          $(this).select2({
            width: 'resolve',
            theme: 'bootstrap4',
            minimumResultsForSearch: -1
          });
          $('.select-group', dis.closest('.form-group')).val(val).trigger('change');
        });
      }
    });
    return false;
  });

  var elEditable = $('[contenteditable=true]');
  if(elEditable) {
    for(var i=0; i<elEditable.length; i++) {
      CKEDITOR.inline(elEditable[i].id, {
        toolbar: [
          { name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', 'Undo', 'Redo' ] },
          { name: 'basicstyles', items: [ 'Subscript', 'Superscript'] }
        ]
      }).on("instanceReady", function(event){
        var ck = this;
        //console.log(this.getData());
        ck.on('blur', function(e){
          var ckval = ck.getData();
          $('[name="'+ck.name+'"]').val(ckval.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "").replace(/(\r\n|\n|\r)/gm,""));
        });
      });
    }

  }

  var modalMedia = $('#modalMedia');
  modalMedia.on('hidden.bs.modal', function (e) {
    $('img.preview', modalMedia).hide();
    $('img.preview', modalMedia).attr('src', '');
    $("[name=FileMedia]", modalMedia).val('').trigger('change');
    $("[name=FileMedia]", modalMedia).next('label').html('PILIH FILE');
  });

  $(".btn-add-media").click(function(){
    var target_ = $(this).closest('.form-group').find('input[type=hidden][name^=QuestImage]');
    if(target_) {
      modalMedia.data('target', target_[0].name);
    }
    modalMedia.modal('show');
  });

  $('#btn-add-media', modalMedia).click(function() {
    var objMedia = {};
    var mediaFile = $('img.preview', modalMedia).attr('src');
    var uniq_ = moment().format('YYYYMMDDHHmmss');
    var target_ = modalMedia.data('target');

    objMedia = {
      Uniq: uniq_,
      ImgPath: mediaFile
    };

    addPostImages(objMedia, target_);
    modalMedia.modal('hide');
  });

  $('[name^=QuestImage]').change(function() {
    var dis = $(this);
    var grp = dis.closest('.form-group');
    var arrPostImages = dis.val();
    if(arrPostImages) {
      arrPostImages = JSON.parse(arrPostImages);
    } else {
      arrPostImages = [];
    }

    if(arrPostImages) {
      $('.preview', $('.div-preview', grp)).remove();
      for(var i=0; i<arrPostImages.length; i++) {
        var imgEl = $('.preview-blueprint', $('.div-preview', grp)).clone();
        imgEl.removeClass('preview-blueprint').removeClass('d-none').addClass('d-flex').addClass('preview');
        if(arrPostImages[i].ImgPath) {
          imgEl.css('background-image', "url('"+arrPostImages[i].ImgPath+"')");
        } else {
          imgEl.css('background-image', "url('"+arrPostImages[i]+"')");
        }
        imgEl.css('background-size', 'cover');

        $('button.btn-del-media', imgEl).data('uniq', arrPostImages[i].Uniq);
        $('button.btn-del-media', imgEl).click(function(){
          var uniq = $(this).data('uniq');
          var arrImg_ = dis.val();
          if(arrImg_) arrImg_ = JSON.parse(arrImg_);
          else arrImg_ = [];

          arrImg_ = arrImg_.filter(function(obj) {
            return obj.Uniq != uniq;
          });

          dis.val(JSON.stringify(arrImg_)).trigger('change');
        });

        if(i==0) {
          $('.div-preview', grp).prepend(imgEl);
        } else {
          $(".div-preview > div.preview:nth-child("+i+")", grp).after(imgEl);
        }
      }
    }
  }).trigger('change');

  $('[name^=QuestType]').change(function() {
    var dis = $(this);
    var type = dis.val();
    var elQuest = dis.closest('.div-quest');

    if(type=='MUL') {
      $('.div-option', elQuest).show();
      $('.div-key', elQuest).hide();
    } else if(type=='TEXT') {
      $('.div-option', elQuest).hide();
      $('.div-key', elQuest).show();
    }
  }).trigger('change');

  $('button.btn-del-media').click(function(){
    var uniq = $(this).data('uniq');
    var arrImg_ = $(this).closest('.form-group').find('input[type=hidden][name^=QuestImage]').val();
    if(arrImg_) arrImg_ = JSON.parse(arrImg_);
    else arrImg_ = [];

    arrImg_ = arrImg_.filter(function(obj) {
      return obj.Uniq != uniq;
    });

    $(this).closest('.form-group').find('input[type=hidden][name^=QuestImage]').val(JSON.stringify(arrImg_)).trigger('change');
  });

  $('.btn-changeval').click(function(){
    var url = $(this).attr('href');
    var val = $(this).data('value');
    var dis = $(this);
    if(val==null) {
      val = '';
    }
    swal({
      closeOnClickOutside: true,
      buttons: ['BATAL','SUBMIT'],
      text: 'UBAH BOBOT',
      content: {
        element: "input",
        attributes: {
          placeholder: 'BOBOT',
          type: "number",
          value: val
        }
      },
    }).then(function(val){
      if(val) {
        dis.closest('.input-group').find('input[name^=VAL__][type=hidden]').val(val);
        dis.text(val);
      }
    });

    return false;
  });

  $('#form-main').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      for (instance in CKEDITOR.instances ) {
        CKEDITOR.instances[instance].updateElement();
      }
      $('[name^=TXT__]').each(function () {
        var _textarea = $(this);
        _textarea.val(CKEDITOR.instances[_textarea.attr('name')].getData());
      });

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
