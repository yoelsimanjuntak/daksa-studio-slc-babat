<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/master/test')?>">Model</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <?php
              if(isset($idTest)) {
                ?>
                <a href="<?=site_url('site/master/question-add/'.$idTest)?>" type="button" class="btn btn-tool btn-add-data text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
                <a href="<?=site_url('site/master/question-delete-bulk')?>" type="button" class="btn btn-tool btn-activation text-danger"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>
                <a href="#" type="button" class="btn btn-tool btn-print-data text-info"><i class="fas fa-print"></i>&nbsp;CETAK</a>
                <a href="<?=site_url('site/master/question-activation-bulk/1')?>" type="button" class="btn btn-tool btn-activation text-success"><i class="fas fa-check"></i>&nbsp;PUBLISH</a>
                <a href="<?=site_url('site/master/question-activation-bulk/-1')?>" type="button" class="btn btn-tool btn-activation text-warning"><i class="fas fa-times"></i>&nbsp;UNPUBLISH</a>
                <?php
              }
              ?>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px"><input type="checkbox" id="checkbox-all" /></th>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>NO.</th>
                    <th>PERTANYAAN</th>
                    <th>OPSI</th>
                    <th>GAMBAR</th>
                    <th>KELOMPOK</th>
                    <th>STATUS</th>
                    <th>DIBUAT PADA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">TAMBAH SOAL <strong><?=strtoupper($title)?></strong></span>
      </div>
      <form action="<?=site_url('site/master/question-add')?>" method="get">
        <div class="modal-body">
          <div class="form-group row mb-0">
            <label class="col-sm-4 control-label">JLH. SOAL</label>
            <div class="col-sm-8">
              <input type="hidden" name="<?=COL_IDTEST?>" value="<?=isset($idTest)?$idTest:''?>" />
              <input type="number" class="form-control text-right" name="num" value="1" />
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-arrow-circle-right"></i>&nbsp;LANJUT</button>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-media" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">MEDIA</span>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-print" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">CETAK SOAL <strong><?=strtoupper($title)?></strong></span>
      </div>
      <form action="<?=site_url('site/master/question-generate/'.$idTest)?>" method="get">
        <div class="modal-body">
          <div class="form-group row">
            <label class="col-sm-4 control-label">JLH. SOAL</label>
            <div class="col-sm-8">
              <input type="number" class="form-control text-right" name="num" value="1" />
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 control-label">STATUS</label>
            <div class="col-sm-8">
              <select class="form-control" name="stat" style="width: 100%">
                <option value="">-- SEMUA --</option>
                <option value="1">UNPUBLISHED</option>
                <option value="2">PUBLISHED</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="offset-sm-4 pl-2 control-label"><input type="checkbox" name="IsRandom" value="1" checked />&nbsp;&nbsp;ACAK SOAL</label>
          </div>
          <div class="form-group row">
            <div class="col-sm-12 mb-2">
              <button type="submit" class="btn btn-sm btn-outline-success btn-block"><i class="far fa-arrow-circle-right"></i>&nbsp;GENERATE</button>
            </div>
            <div class="col-sm-6 div-download">
              <a href="" id="download1" class="btn btn-sm btn-outline-primary btn-block d-none" target="_blank"><i class="far fa-download"></i>&nbsp;SOAL</a>
            </div>
            <div class="col-sm-6 div-download">
              <a href="" id="download2" class="btn btn-sm btn-outline-primary btn-block d-none" target="_blank"><i class="far fa-download"></i>&nbsp;SOAL + KUNCI</a>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterStatus" style="width: 200px">
    <option value="">Status</option>
    <option value="1" <?=!empty($_GET['filterStatus'])&&$_GET['filterStatus']==1?'selected':''?>>Published</option>
    <option value="-1" <?=!empty($_GET['filterStatus'])&&$_GET['filterStatus']==-1?'selected':''?>>Unpublished</option>
  </select>
</div>
<script type="text/javascript">
var dt = null;
$(document).ready(function() {
  dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/master/question-load/'.(isset($idTest)?$idTest:999))?>",
      "type": 'POST',
      "data": function(data){
        //data.filterStart = $('[name=filterStart]', $('.filtering')).val();
        data.filterStatus = $('[name=filterStatus]', $('.filtering')).val();
       }
    },
    "scrollY" : '39vh',
    "scrollX": "200%",
    //"deferRender": true,
    //"scrollCollapse": true,
    //"scroller": true,
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [],
    "columnDefs": [
      {"targets":[1,4,5,7], "className":'nowrap text-center'},
      {"targets":[2,8], "className":'nowrap dt-body-right'},
      {"targets":[3], "className":'nowrap'}
    ],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": false,"width": "50px"},
      {"orderable": false,"width": "10px"},
      {"orderable": false},
      {"orderable": false,"width": "10px"},
      {"orderable": false,"width": "10px"},
      {"orderable": false,"width": "10px"},
      {"orderable": false,"width": "10px"},
      {"orderable": false,"width": "10px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        $.get(url, function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
          }
        }, "json").done(function() {
          dt.DataTable().ajax.reload();
          //location.reload();
        }).fail(function() {
          toastr.error('SERVER ERROR');
        });
        return false;
      });
      $('[data-toggle="tooltip"]', $(row)).tooltip();
      $('.btn-popup-modal', $(row)).click(function() {
        var href = $(this).attr('href');
        var modal = $(this).data('target');
        $('.modal-body', $(modal)).load(href, function() {
          $(modal).modal('show');
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
      dt.DataTable().page('last').draw('page');
    }
  });

  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
    //location.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });
  $('.btn-add-data').click(function() {
    $('#modal-add').modal('show');
    return false;
  });
  $('.btn-print-data').click(function() {
    $('#modal-print').modal('show');
    return false;
  });

  $('form', $('#modal-print')).submit(function(){
    var form = $(this);
    var btnSubmit = $('button[type=submit]', $('form', $('#modal-print')));
    var num = $('[name=num]', $('#modal-print')).val();
    var mode = $('[name=mode]', $('#modal-print')).val();

    btnSubmit.attr('disabled', true);
    form.ajaxSubmit({
      dataType: 'json',
      type : 'post',
      success: function(res) {
        if(res.error) {
          toastr.error(res.error);
          btnSubmit.attr('disabled', false);
        } else {
          $('#download1').attr('href', res.URL1).removeClass('d-none');
          $('#download2').attr('href', res.URL2).removeClass('d-none');
        }
      },
      error: function() {
        toastr.error('SERVER ERROR!');
        btnSubmit.attr('disabled', false);
      },
      complete: function() {
        //btnSubmit.attr('disabled', false);
      }
    });
    return false;
  });

  $('#modal-print').on('hidden.bs.modal', function (e) {
    $('button[type=submit]', $('form', $('#modal-print'))).attr('disabled', false);
    $('#download1, #download2').attr('href', '').addClass('d-none');
  });

  <?php
  if(!empty($_GET['scrollTo'])) {
    ?>
    var rowUniq = <?=$_GET['scrollTo']?>;
    var dt_ = dt.DataTable();
    dt_.row('[data-uniq='+rowUniq+']').scrollTo();
    <?php
  }
  ?>

  $('#checkbox-all').click(function() {
    if($(this).is(':checked')){
      $('.checkbox-item').prop('checked',true);
    }else{
      $('.checkbox-item').prop('checked',false);
    }
  });
  $('.btn-activation').click(function() {
    var selected = $('.checkbox-item:checked');
    var a = $(this);
    if (selected.length>0) {
      if(confirm(selected.length+' data dipilih. Apakah anda yakin ingin melanjutkan?')) {
        $('#dataform').ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data) {
            if(data.error==0){
              toastr.success(data.success);
              dt.DataTable().ajax.reload();
            }else{
              toastr.error(data.error);
            }
          },
          complete: function(){
            dt.DataTable().ajax.reload();
            $('#checkbox-all').prop('checked', false).trigger('change');
          }
        });
      }
    } else {
      alert('Belum ada data yang dipilih!');
    }
    return false;
  });
});
</script>
