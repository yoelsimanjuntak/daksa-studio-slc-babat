<h5 class="font-weight-bold" style="text-decoration: underline"><?=$modul[COL_MODTITLE]?></h5>
<p style=""="text-align: justify">
  <?=$modul[COL_MODDESC]?>
</p>
<hr />
<p class="text-right mb-0">
  <a href="<?=base_url().'assets/media/modules/'.$modul[COL_MODFILENAME]?>" target="_blank" class="btn btn-sm btn-outline-primary"><i class="far fa-download"></i>&nbsp;DOWNLOAD</a>
</p>
