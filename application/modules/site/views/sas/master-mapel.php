<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/sas/master-mapel-add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>KATEGORI</th>
                    <th>MATA PELAJARAN</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">MATA PELAJARAN</span>
      </div>
      <form id="form-kategori" action="" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label>KATEGORI</label>
            <select class="form-control" name="<?=COL_MAPELKATEGORI?>" style="width: 100% !important">
              <option value="IPA">IPA</option>
              <option value="IPS">IPS</option>
              <option value="BAHASA">BAHASA</option>
              <option value="SMK IPA">SMK IPA</option>
              <option value="SMK IPS">SMK IPS</option>
              <option value="PENDUKUNG">PENDUKUNG</option>
            </select>
          </div>
          <div class="form-group mb-0">
            <label>MATA PELAJARAN</label>
            <input type="text" class="form-control" name="<?=COL_MAPELNAMA?>" />
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/sas/master-mapel-load')?>",
      "type": 'POST',
      "data": function(data){

       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [[ 1, "asc" ]],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('[data-toggle="tooltip"]', $(row)).tooltip();
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-edit', $(row)).click(function(){
        var a = $(this);
        var kategori = $(this).data('kategori');
        var name = $(this).data('name');
        var editor = $("#modal-form");

        $('option[value='+kategori+']', $('[name=<?=COL_MAPELKATEGORI?>]', editor)).prop('selected', true);
        $('[name=<?=COL_MAPELKATEGORI?>]', editor).trigger('change');
        $('[name=<?=COL_MAPELNAMA?>]', editor).val(name);

        editor.modal("show");
        $("button[type=submit]", editor).unbind('click').click(function() {
          var dis = $(this);
          dis.html("Loading...").attr("disabled", true);
          $('#form-kategori').ajaxSubmit({
            dataType: 'json',
            url : a.attr('href'),
            success : function(data){
              if(data.error==0){
                dt.DataTable().ajax.reload();
              }else{
                toastr.error(data.error);
              }
            },
            complete: function(data) {
              dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
              $('input', editor).val('');
              editor.modal("hide");
            }
          });
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });

  $('.btn-add').click(function(){
    var a = $(this);
    var editor = $("#modal-form");

    editor.modal("show");
    $("button[type=submit]", editor).unbind('click').click(function() {
      var dis = $(this);
      dis.html("Loading...").attr("disabled", true);
      $('#form-kategori').ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            dt.DataTable().ajax.reload();
          }else{
            toastr.error(data.error);
          }
        },
        complete: function(data) {
          dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
          $('input', editor).val('');
          editor.modal("hide");
        }
      });
    });
    return false;
  });

  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    $('#modal-add').modal('show');
    return false;
  });
});
</script>
