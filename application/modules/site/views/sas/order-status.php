<form action="<?=current_url()?>" method="post">
  <div class="form-group">
    <label class="control-label">STATUS</label>
    <select class="form-control" name="<?=COL_ORDERSTATUS?>" style="width: 100%">
      <option value="NEW" <?=!empty($data)&&$data[COL_ORDERSTATUS]=='NEW'?'selected':''?>>NEW</option>
      <option value="APPROVED" <?=!empty($data)&&$data[COL_ORDERSTATUS]=='APPROVED'?'selected':''?>>APPROVED</option>
      <option value="REJECTED" <?=!empty($data)&&$data[COL_ORDERSTATUS]=='REJECTED'?'selected':''?>>REJECTED</option>
    </select>
  </div>
</form>
