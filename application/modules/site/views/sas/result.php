<?php
$rdet = $this->db
->select('sas_tformdetail.*, sas_mmapel.MapelNama')
->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
->where(COL_IDFORM, $data[COL_UNIQ])
->get(TBL_SAS_TFORMDETAIL)
->result_array();

$rdetsum = $this->db
->select('AVG(sas_tformdetail.NumNR) as AvgNilai')
->where(COL_IDFORM, $data[COL_UNIQ])
->get(TBL_SAS_TFORMDETAIL)
->row_array();

$rdetmin = $this->db
->select('sas_tformdetail.*, sas_mmapel.MapelNama')
->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
->where(TBL_SAS_TFORMDETAIL.'.'.COL_IDFORM, $data[COL_UNIQ])
->order_by(TBL_SAS_TFORMDETAIL.'.'.COL_NUMNR,'asc')
->get(TBL_SAS_TFORMDETAIL)
->row_array();

$rdetmax = $this->db
->select('sas_tformdetail.*, sas_mmapel.MapelNama')
->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
->where(TBL_SAS_TFORMDETAIL.'.'.COL_IDFORM, $data[COL_UNIQ])
->order_by(TBL_SAS_TFORMDETAIL.'.'.COL_NUMNR,'desc')
->get(TBL_SAS_TFORMDETAIL)
->row_array();

$numNR = !empty($rdetsum)?$rdetsum['AvgNilai']:0;
$numNilaiPerc = 0;
$numAkreditasi = 0;
$numDomisiliPT1 = 0;
$numDomisiliPT2 = 0;
$numAlumni = 0;
$numRankSekolah = 0;
$numRankSiswa = 0;
$numMapelPendukung = 0;
$numPiagam = 0;
$numPortfolio = 0;
$txtPeluang = '';
$numGrade = 0;
$numLevel = 0;

$arrResult = array();

if($data[COL_FORMPORTOFOLIO]=='ADA') {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'NILAI_WITH_PORTOFOLIO')
  ->where(COL_MATRANGEFROM.'<=',$numNR)
  ->where(COL_MATRANGETO.'>=',$numNR)
  ->order_by(COL_MATRANGEFROM,'desc')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();

  $numNilaiPerc = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numNilaiPerc;
} else if($data[COL_FORMPORTOFOLIO]=='TIDAK') {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'NILAI_NO_PORTOFOLIO')
  ->where(COL_MATRANGEFROM.'<=',$numNR)
  ->where(COL_MATRANGETO.'>=',$numNR)
  ->order_by(COL_MATRANGEFROM,'desc')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();

  $numNilaiPerc = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numNilaiPerc;
}

$rmatriks = $this->db
->where(COL_MATNAME,'AKREDITASI')
->where(COL_MATVAL,$data[COL_FORMAKREDITAS])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numAkreditasi = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numAkreditasi;

if($data[COL_IDPROVINSI]==$data[COL_IDPROVINSIPT1]) {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN1')
  ->where(COL_MATVAL,'YA')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT1 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT1;
} else {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN1')
  ->where(COL_MATVAL,'TIDAK')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT1 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT1;
}

if($data[COL_IDPROVINSI]==$data[COL_IDPROVINSIPT2]) {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN2')
  ->where(COL_MATVAL,'YA')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT2 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT2;
} else {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN2')
  ->where(COL_MATVAL,'TIDAK')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT2 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT2;
}

$rmatriks = $this->db
->where(COL_MATNAME,'ALUMNI_SNBP')
->where(COL_MATVAL,$data[COL_FORMNUMALUMNI])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numAlumni = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numAlumni;

$rmatriks = $this->db
->where(COL_MATNAME,'RANKING_SEKOLAH')
->where(COL_MATVAL,$data[COL_FORMRANKSEKOLAH])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numRankSekolah = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numRankSekolah;

$rmatriks = $this->db
->where(COL_MATNAME,'RANK_PARALEL')
->where(COL_MATVAL,$data[COL_FORMRANKPARALEL])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numRankSiswa = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numRankSiswa;

$rmatriks = $this->db
->where(COL_MATNAME,'MAPEL_PENDUKUNG')
->where(COL_MATVAL,$data[COL_FORMMAPELKESESUAIAN])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numMapelPendukung = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numMapelPendukung;

$rmatriks = $this->db
->where(COL_MATNAME,'PIAGAM')
->where(COL_MATVAL,$data[COL_FORMPIAGAM])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numPiagam = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numPiagam;

$rmatriks = $this->db
->where(COL_MATNAME,'PORTOFOLIO')
->where(COL_MATVAL,$data[COL_FORMPORTOFOLIO])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numPortfolio = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numPortfolio;

$rmatriks = $this->db
->where(COL_MATNAME,'PELUANG')
->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
->where(COL_MATRANGETO.'>=',array_sum($arrResult))
->order_by(COL_MATRANGEFROM,'desc')
->get(TBL_SAS_MMATRIKS)
->row_array();
$txtPeluang = !empty($rmatriks)?$rmatriks[COL_MATVAL]:'';

$rmatriksGrade = $this->db
->where(COL_MATNAME,'ANALYSIS_GRADE')
->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
->where(COL_MATRANGETO.'>=',array_sum($arrResult))
->order_by(COL_MATRANGEFROM,'desc')
->get(TBL_SAS_MMATRIKS)
->row_array();
$numGrade = !empty($rmatriksGrade)?$rmatriksGrade[COL_MATPOINT]:0;

$rmatriksLevel = $this->db
->where(COL_MATNAME,'ANALYSIS_LEVEL')
->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
->where(COL_MATRANGETO.'>=',array_sum($arrResult))
->order_by(COL_MATRANGEFROM,'desc')
->get(TBL_SAS_MMATRIKS)
->row_array();
$numLevel = !empty($rmatriksLevel)?$rmatriksLevel[COL_MATPOINT]:0;

$rptn = $this->db
->where(COL_PRODITIPE, $data[COL_FORMPROGRAM])
->where(COL_NUMNILAI." >= ".$rmatriksGrade[COL_MATRANGEFROM])
->where(COL_NUMNILAI." <= ".$rmatriksGrade[COL_MATRANGETO])
->order_by(COL_NUMNILAI,'desc')
->limit(10)
->get(TBL_SAS_MPRODI)
->result_array();

$rptn1 = $this->db
->where(COL_PRODINAMA, $data['ProdiNama1'])
->where(COL_PRODITIPE, $data[COL_FORMPROGRAM])
->where(COL_NUMNILAI." >= ".$rmatriksGrade[COL_MATRANGEFROM])
->where(COL_NUMNILAI." <= ".$rmatriksGrade[COL_MATRANGETO])
->order_by(COL_NUMNILAI,'desc')
->limit(5)
->get(TBL_SAS_MPRODI)
->result_array();

$rptn2 = $this->db
->where(COL_PRODINAMA, $data['ProdiNama2'])
->where(COL_PRODITIPE, $data[COL_FORMPROGRAM])
->where(COL_NUMNILAI." >= ".$rmatriksGrade[COL_MATRANGEFROM])
->where(COL_NUMNILAI." <= ".$rmatriksGrade[COL_MATRANGETO])
->order_by(COL_NUMNILAI,'desc')
->limit(5)
->get(TBL_SAS_MPRODI)
->result_array();
?>
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-8">
        <h5 class="m-0 text-dark font-weight-bold"> <?=$title?></h5>
        <p class="text-sm font-italic mb-0">Hasil perhitungan peluang kelulusan SNBP siswa</p>
      </div>
      <div class="col-sm-4 text-right">
        <a href="<?=site_url('site/sas/result/'.$data[COL_UNIQ].'/print')?>" target="_blank" class="btn btn-outline-success btn-print" style="background-color: #fff"><i class="far fa-print"></i> CETAK HASIL</a>
      </div>
    </div>
  </div>
</div>
<div class="content" id="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="info-box mb-4 bg-success">
          <span class="info-box-icon"><i class="far fa-chart-network"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Perkiraan Pencapaian</span>
            <span class="info-box-number text-lg"><?=number_format(array_sum($arrResult))?> - <?=$txtPeluang?></span>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="info-box mb-4 bg-success">
          <span class="info-box-icon"><i class="far fa-chart-bar"></i></span>
          <div class="info-box-content">
            <span class="info-box-text">Prodi yang Mendekati Nilai Anda</span>
            <span class="info-box-number text-lg">LEVEL <?=$numLevel?></span>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="card card-info mb-4">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">DATA PERSONAL</h5>
          </div>
          <div class="card-body p-0">
            <ul class="nav flex-column">
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Nama</span> <strong><?=$data[COL_FORMNAMA]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Jenis Kelamin</span> <strong><?=$data[COL_FORMJK]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Asal Sekolah</span> <strong><?=$data[COL_FORMASALSEKOLAH]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Domisili</span> <strong><?=$data['Provinsi']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Akreditas Sekolah</span> <strong><?=$data[COL_FORMAKREDITAS]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Jurusan Sekolah</span> <strong><?=$data[COL_FORMJURUSAN]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Peminatan Program</span> <strong><?=$data[COL_FORMPROGRAM]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Domisili PTN 1</span> <strong><?=$data['ProvinsiPTN1']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Domisili PTN 2</span> <strong><?=$data['ProvinsiPTN2']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Alumni 1 Tahun Terakhir</span> <strong><?=$data[COL_FORMNUMALUMNI]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Ranking Sekolah</span> <strong><?=$data[COL_FORMRANKSEKOLAH]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Ranking Paralel</span> <strong><?=$data[COL_FORMRANKPARALEL]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Kesesuaian Mapel Pendukung</span> <strong><?=$data[COL_FORMMAPELKESESUAIAN]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Sertifikat / Piagam</span> <strong><?=$data[COL_FORMPIAGAM]?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Memiliki Portfolio</span> <strong><?=$data[COL_FORMPORTOFOLIO]?></strong>
                </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="card card-info mb-4">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">NILAI RATA-RATA</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-bordered table-condensed text-sm">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Mata Pelajaran</th>
                  <th>Nilai</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no=1;
                foreach ($rdet as $r) {
                  ?>
                  <tr>
                    <td style="width: 10px; white-space: nowrap"><?=$no?></td>
                    <td><?=$r[COL_MAPELNAMA]?></td>
                    <td class="text-center"><?=$r[COL_NUMNR]?></td>
                  </tr>
                  <?php
                  $no++;
                }
                ?>
              </tbody>
              <tfoot>
                <tr>
                  <th colspan="2" class="text-center">RATA-RATA</th>
                  <th class="text-center"><?=number_format($numNR, 2)?></th>
                </tr>
              </tfoot>
            </table>
          </div>
          <div class="card-footer p-0">
            <ul class="nav flex-column">
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Mapel Terbaik</span> <strong><?=!empty($rdetmax)?$rdetmax[COL_MAPELNAMA]:'-'?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Mapel Terendah</span> <strong><?=!empty($rdetmin)?$rdetmin[COL_MAPELNAMA]:'-'?></strong>
                </span>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-primary mb-4">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">PILIHAN PTN 1</h5>
          </div>
          <div class="card-body p-0">
            <ul class="nav flex-column">
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Kode Prodi</span> <strong><?=$data['ProdiKode1']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Nama PTN</span> <strong><?=$data['ProdiPT1']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Nama Prodi</span> <strong><?=$data['ProdiNama1']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Prediksi Nilai</span> <strong><?=number_format($data['ProdiNilai1'], 2)?></strong>
                </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="card card-primary mb-4">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">PILIHAN PTN 2</h5>
          </div>
          <div class="card-body p-0">
            <ul class="nav flex-column">
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Kode Prodi</span> <strong><?=$data['ProdiKode2']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Nama PTN</span> <strong><?=$data['ProdiPT2']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Nama Prodi</span> <strong><?=$data['ProdiNama2']?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Prediksi Nilai</span> <strong><?=number_format($data['ProdiNilai2'], 2)?></strong>
                </span>
              </li>
            </ul>
          </div>
        </div>
        <div class="card card-primary mb-4 d-none">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">SKORING</h5>
          </div>
          <div class="card-body p-0">
            <ul class="nav flex-column">
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Persentase Nilai</span> <strong><?=$numNilaiPerc?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Akreditasi</span> <strong><?=$numAkreditasi?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Domisili PTN 1</span> <strong><?=$numDomisiliPT1?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Domisili PTN 2</span> <strong><?=$numDomisiliPT2?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Alumni</span> <strong><?=$numAlumni?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Rank Sekolah</span> <strong><?=$numRankSekolah?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Rank Paralel</span> <strong><?=$numRankSiswa?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Mapel Pendukung</span> <strong><?=$numMapelPendukung?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Sertifikat / Piagam</span> <strong><?=$numPiagam?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Portfolio</span> <strong><?=$numPortfolio?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm text-primary" style="padding: .25rem 1rem !important">
                  <span class="float-left">Peluang (%)</span> <strong><?=array_sum($arrResult)?></strong>
                </span>
              </li>
              <li class="nav-item text-right text-primary">
                <span class="nav-link text-sm text-primary" style="padding: .25rem 1rem !important">
                  <span class="float-left">Peluang</span> <strong><?=$txtPeluang?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Grade</span> <strong><?=$numGrade?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Level</span> <strong><?=$numLevel?></strong>
                </span>
              </li>
              <li class="nav-item text-right">
                <span class="nav-link text-sm" style="padding: .25rem 1rem !important">
                  <span class="float-left">Kategori</span> <strong><?=$data[COL_FORMPROGRAM].$numGrade?></strong>
                </span>
              </li>
            </ul>
          </div>
        </div>

        <div class="card card-primary mb-4">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">SARAN PTN BERDASARKAN PILIHAN</h5>
          </div>
          <div class="card-body p-0">
            <div class="table-responsive">
              <table class="table table-bordered table-condensed text-sm mb-0">
                <thead>
                  <tr class="text-center">
                    <th style="vertical-align: middle">Kode</th>
                    <th style="vertical-align: middle">Prodi</th>
                    <th style="vertical-align: middle">PTN</th>
                    <th style="vertical-align: middle">Prediksi Nilai</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no=1;
                  foreach ($rptn1 as $r) {
                    ?>
                    <tr>
                      <td class="text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_PRODIKODE]?></td>
                      <td style="white-space: nowrap"><?=$r[COL_PRODINAMA]?></td>
                      <td style="white-space: nowrap"><?=$r[COL_PRODIPT]?></td>
                      <td class="text-center" style="width: 10px; white-space: nowrap"><?=number_format($r[COL_NUMNILAI],2)?></td>
                    </tr>
                    <?php
                    $no++;
                  }
                  ?>
                  <?php
                  $no=1;
                  foreach ($rptn2 as $r) {
                    ?>
                    <tr>
                      <td class="text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_PRODIKODE]?></td>
                      <td style="white-space: nowrap"><?=$r[COL_PRODINAMA]?></td>
                      <td style="white-space: nowrap"><?=$r[COL_PRODIPT]?></td>
                      <td class="text-center" style="width: 10px; white-space: nowrap"><?=number_format($r[COL_NUMNILAI],2)?></td>
                    </tr>
                    <?php
                    $no++;
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="card card-primary mb-4">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">SARAN PTN BERDASARKAN NILAI</h5>
          </div>
          <div class="card-body p-0">
            <div class="table-responsive">
              <table class="table table-bordered table-condensed text-sm mb-0">
                <thead>
                  <tr class="text-center">
                    <th style="vertical-align: middle">Kode</th>
                    <th style="vertical-align: middle">Prodi</th>
                    <th style="vertical-align: middle">PTN</th>
                    <th style="vertical-align: middle">Prediksi Nilai</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no=1;
                  foreach ($rptn as $r) {
                    ?>
                    <tr>
                      <td class="text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_PRODIKODE]?></td>
                      <td style="white-space: nowrap"><?=$r[COL_PRODINAMA]?></td>
                      <td style="white-space: nowrap"><?=$r[COL_PRODIPT]?></td>
                      <td class="text-center" style="width: 10px; white-space: nowrap"><?=number_format($r[COL_NUMNILAI],2)?></td>
                    </tr>
                    <?php
                    $no++;
                  }
                  ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
