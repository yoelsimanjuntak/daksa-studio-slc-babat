<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-8">
        <h5 class="m-0 text-dark font-weight-bold"> <?=$title?></h5>
        <p class="text-sm font-italic mb-0">Silakan mengisi form berikut ini terlebih dahulu.</p>
      </div>
      <div class="col-sm-4 text-right">
        <button type="submit" class="btn btn-primary">SUBMIT & LIHAT HASIL <i class="far fa-arrow-circle-right"></i></button>
      </div>
    </div>
  </div>
</div>
<div class="content">
  <div class="container">
    <form id="form-main" action="<?=current_url()?>" method="post">
      <div class="row">
        <div class="col-lg-12">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h5 class="card-title m-0 font-weight-bold">DATA DIRI</h5>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6 col-12">
                  <?php
                  if($ruser[COL_ROLEID]==ROLEADMIN) {
                    ?>
                    <div class="form-group">
                      <div class="row">
                        <label class="control-label col-lg-4 col-12">Pengguna</label>
                        <div class="col-lg-8 col-12">
                          <select class="form-control" name="<?=COL_USERNAME?>" style="width: 100%" required>
                            <?=GetCombobox("select * from users where RoleID=".ROLEUSER." order by Fullname", COL_USERNAME, array(COL_FULLNAME, COL_USERNAME))?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <?php
                  } else {
                    ?>
                    <div class="form-group">
                      <div class="row">
                        <label class="control-label col-lg-4 col-12">Nama Lengkap</label>
                        <div class="col-lg-8 col-12">
                          <input type="text" name="<?=COL_FORMNAMA?>" class="form-control" style="text-transform: uppercase" value="<?=$ruser[COL_ROLEID]!=ROLEADMIN?$ruser[COL_FULLNAME]:''?>" required />
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                  ?>
                  <div class="form-group">
                    <div class="row">
                      <label class="control-label col-lg-4 col-12">Jenis Kelamin</label>
                      <div class="col-lg-8 col-12">
                        <select class="form-control" name="<?=COL_FORMJK?>" style="width: 100%" required>
                          <option value="LAKI-LAKI">LAKI-LAKI</option>
                          <option value="PEREMPUAN">PEREMPUAN</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <label class="control-label col-lg-4 col-12">Asal Sekolah</label>
                      <div class="col-lg-8 col-12">
                        <input type="text" name="<?=COL_FORMASALSEKOLAH?>" class="form-control" style="text-transform: uppercase" placeholder="SMA N 1 JAKARTA" required />
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <label class="control-label col-lg-4 col-12">Provinsi Sekolah</label>
                      <div class="col-lg-8 col-12">
                        <select class="form-control" name="<?=COL_IDPROVINSI?>" style="width: 100%" required>
                          <?=GetCombobox("select * from sas_mprovinsi order by Provinsi", COL_UNIQ, COL_PROVINSI)?>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-12">
                  <div class="form-group">
                    <div class="row">
                      <label class="control-label col-lg-4 col-12">Jurusan Sekolah</label>
                      <div class="col-lg-8 col-12">
                        <select class="form-control" name="<?=COL_FORMJURUSAN?>" style="width: 100%" required>
                          <option value="IPA">IPA</option>
                          <option value="IPS">IPS</option>
                          <option value="BAHASA">BAHASA</option>
                          <option value="SMK IPA">SMK (IPA)</option>
                          <option value="SMK IPS">SMK (IPS)</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <label class="control-label col-lg-4 col-12">Akreditas Sekolah</label>
                      <div class="col-lg-8 col-12">
                        <select class="form-control" name="<?=COL_FORMAKREDITAS?>" style="width: 100%" required>
                          <?=GetCombobox("select * from sas_mmatriks where MatName='AKREDITASI'", COL_MATVAL, COL_MATVAL)?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <label class="control-label col-lg-4 col-12">Ranking Sekolah</label>
                      <div class="col-lg-8 col-12">
                        <div class="input-group">
                          <select class="form-control" name="<?=COL_FORMRANKSEKOLAH?>" required>
                            <?=GetCombobox("select * from sas_mmatriks where MatName='RANKING_SEKOLAH'", COL_MATVAL, COL_MATVAL)?>
                          </select>
                          <span class="input-group-append">
                            <a href="https://top-1000-sekolah.ltmpt.ac.id/" target="_blank" class="btn btn-primary"><i class="far fa-search"></i> CEK</a>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <label class="control-label col-lg-4 col-12">Jlh. Alumni</label>
                      <div class="col-lg-8 col-12">
                        <select class="form-control" name="<?=COL_FORMNUMALUMNI?>" style="width: 100%" required>
                          <?=GetCombobox("select * from sas_mmatriks where MatName='ALUMNI_SNBP'", COL_MATVAL, COL_MATVAL)?>
                        </select>
                        <p class="text-sm text-muted font-italic mt-1">Alumni sekolah 1 tahun terakhir yang diterima melalui jalur SNBP di PTN pilihan</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row d-flex align-items-stretch pb-5">
        <div class="col-lg-6">
          <div class="card card-primary h-100">
            <div class="card-header">
              <h5 class="card-title m-0 font-weight-bold">PILIHAN & PRESTASI</h5>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label class="control-label">Peminatan</label>
                <select class="form-control" name="<?=COL_FORMPROGRAM?>" style="width: 100%" required>
                  <option value="SAINTEK">SAINTEK</option>
                  <option value="SOSHUM">SOSHUM</option>
                </select>
              </div>

              <div class="form-group">
                <label class="control-label">Pilihan PTN</label>
                <div class="input-group">
                  <span class="input-group-prepend">
                    <span class="input-group-text font-weight-bold bg-primary">1</span>
                  </span>
                  <input type="hidden" name="<?=COL_IDPRODI1?>" required />
                  <input type="text" class="form-control" placeholder="PTN / PRODI PILIHAN 1" name="PT1" readonly required />
                  <span class="input-group-append">
                    <a href="<?=site_url('site/sas/browse-prodi')?>" class="btn btn-primary btn-browse-prodi" data-target="<?=COL_IDPRODI1?>" data-target-nama="PT1"><i class="far fa-search"></i></a>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-prepend">
                    <span class="input-group-text font-weight-bold bg-primary">2</span>
                  </span>
                  <input type="hidden" name="<?=COL_IDPRODI2?>" required />
                  <input type="text" class="form-control" placeholder="PTN / PRODI PILIHAN 2" name="PT2" readonly required />
                  <span class="input-group-append">
                    <a href="<?=site_url('site/sas/browse-prodi')?>" class="btn btn-primary btn-browse-prodi" data-target="<?=COL_IDPRODI2?>" data-target-nama="PT2"><i class="far fa-search"></i></a>
                  </span>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6 col-12">
                    <label class="control-label">Provinsi PTN 1</label>
                    <select class="form-control" name="<?=COL_IDPROVINSIPT1?>" style="width: 100%" required>
                      <?=GetCombobox("select * from sas_mprovinsi order by Provinsi", COL_UNIQ, COL_PROVINSI)?>
                    </select>
                  </div>
                  <div class="col-lg-6 col-12">
                    <label class="control-label">Provinsi PTN 2</label>
                    <select class="form-control" name="<?=COL_IDPROVINSIPT2?>" style="width: 100%" required>
                      <?=GetCombobox("select * from sas_mprovinsi order by Provinsi", COL_UNIQ, COL_PROVINSI)?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label">Mapel Pendukung / Nilai</label>
                <div class="row">
                  <div class="col-lg-8 col-12">
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text font-weight-bold bg-primary">1</span>
                      </span>
                      <select class="form-control" name="<?=COL_IDMAPEL1?>" required>
                        <?=GetCombobox("select * from sas_mmapel where MapelKategori='PENDUKUNG'", COL_UNIQ, COL_MAPELNAMA)?>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-4 col-12">
                    <input type="number" class="form-control" placeholder="Nilai Rata-Rata" name="<?=COL_FORMMAPELNILAI1?>" required />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-8 col-12">
                    <div class="input-group">
                      <span class="input-group-prepend">
                        <span class="input-group-text font-weight-bold bg-primary">2</span>
                      </span>
                      <select class="form-control" name="<?=COL_IDMAPEL2?>" required>
                        <?=GetCombobox("select * from sas_mmapel where MapelKategori='PENDUKUNG'", COL_UNIQ, COL_MAPELNAMA)?>
                      </select>
                    </div>
                  </div>
                  <div class="col-lg-4 col-12">
                    <input type="number" class="form-control" placeholder="Nilai Rata-Rata" name="<?=COL_FORMMAPELNILAI2?>" required />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6 col-12">
                    <label class="control-label">Kesesuaian Mapel Pendukung</label>
                    <select class="form-control" name="<?=COL_FORMMAPELKESESUAIAN?>" style="width: 100%" required>
                      <?=GetCombobox("select * from sas_mmatriks where MatName='MAPEL_PENDUKUNG'", COL_MATVAL, COL_MATVAL)?>
                    </select>
                  </div>
                  <div class="col-lg-6 col-12">
                    <label class="control-label">Ranking Paralel</label>
                    <select class="form-control" name="<?=COL_FORMRANKPARALEL?>" style="width: 100%" required>
                      <?=GetCombobox("select * from sas_mmatriks where MatName='RANK_PARALEL'", COL_MATVAL, COL_MATVAL)?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-lg-6 col-12">
                    <label class="control-label">Piagam / Sertifikat</label>
                    <select class="form-control" name="<?=COL_FORMPIAGAM?>" style="width: 100%" required>
                      <?=GetCombobox("select * from sas_mmatriks where MatName='PIAGAM'", COL_MATVAL, COL_MATVAL)?>
                    </select>
                  </div>
                  <div class="col-lg-6 col-12">
                    <label class="control-label">Portofolio</label>
                    <select class="form-control" name="<?=COL_FORMPORTOFOLIO?>" style="width: 100%" required>
                      <?=GetCombobox("select * from sas_mmatriks where MatName='PORTOFOLIO'", COL_MATVAL, COL_MATVAL)?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <ul class="mb-0 text-sm font-italic">
                <li>Rangking Paralel Siswa <strong>Tanpa Portofolio</strong> = Jurusan yang kamu pilih <strong>tidak perlu menyertakan</strong> portofolio.</li>
                <li>Rangking Paralel Siswa <strong>Dengan Portofolio</strong> = Jurusan yang kamu pilih <strong>perlu menyertakan</strong> portofolio.</li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card card-primary h-100" id="card-nilai">
            <div class="card-header">
              <h5 class="card-title m-0 font-weight-bold">RATA-RATA NILAI RAPOR</h5>
            </div>
            <div class="card-body">

            </div>
            <div class="card-footer">
              <ul class="mb-0 text-sm font-italic">
                <li><strong>Rata-Rata Nilai</strong> Rapor = Hasil penjumlahan nilai <strong>semester 1 s/d 5</strong> lalu <strong>dibagi 5</strong> dan <strong>dibulatkan</strong>.</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<div class="modal fade" id="modal-browse-prodi" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title font-weight-bold">DAFTAR PTN DAN PRODI</h6>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('.btn-browse-prodi').click(function() {
    var href = $(this).attr('href');
    var target = $(this).data('target');
    var targetnama = $(this).data('target-nama');
    $('.modal-body', $('#modal-browse-prodi')).html('<p class="text-center">MEMUAT DATA<br /><i class="far fa-spinner fa-spin"></i></p>');
    $('#modal-browse-prodi').modal('show');
    $('.modal-body', $('#modal-browse-prodi')).load(href, function(){
      var dt = $('#datalist', $('#modal-browse-prodi')).dataTable({
        "autoWidth" : false,
        "iDisplayLength": 10,
        //"scrollY" : '40vh',
        "oLanguage": {
          "sSearch": "FILTER "
        },
        //"dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
        "order": [],
        "columnDefs": [
          {"targets":[0,3], "className":'nowrap text-center'}
        ],
        "columns": [
          {"orderable": false,"width": "50px"},
          {"orderable": true},
          {"orderable": true},
          {"orderable": false}
        ],
        "createdRow": function(row, data, dataIndex) {
          $('.btn-select', $(row)).click(function(){
            var uniq = $(this).data('uniq');
            var desc = $(this).data('desc');

            $('[name='+target+']').val(uniq);
            $('[name='+targetnama+']').val(desc);

            $('#datalist').closest('#modal-browse-prodi').modal('hide');
          });
        },
        "initComplete": function(settings, json) {
          $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
        }
      });
    });
    return false;
  });

  $('[name=FormJurusan]').change(function(){
    var val = $(this).val();
    $('.card-body', $('#card-nilai')).load('<?=site_url('site/sas/load-form-nilai')?>',{Jurusan: val},function(){

    });
  }).trigger('change');

  $("form#form-main").validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]');
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i> <span class="font-italic">Loading ...</span>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.href = '<?=site_url('site/user/dashboard')?>';
            },2000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('button[type=submit]').click(function(){
    if(confirm('Apakah anda yakin ingin mengirim data yang sudah anda isi dibawah?')) {
      $('form#form-main').submit();
    }
  });
});
</script>
