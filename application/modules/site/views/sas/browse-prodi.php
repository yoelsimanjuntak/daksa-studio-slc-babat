<table id="datalist" class="table table-bordered table-condensed">
  <thead class="text-sm">
    <tr>
      <th>KODE</th>
      <th>PTN</th>
      <th>PRODI</th>
      <th class="text-center" style="width: 10px">#</th>
    </tr>
  </thead>
  <tbody class="text-sm">
    <?php
    foreach($res as $r) {
      ?>
      <tr>
        <td><?=$r[COL_PRODIKODE]?></td>
        <td><?=$r[COL_PRODIPT]?></td>
        <td><?=$r[COL_PRODINAMA]?></td>
        <td>
          <button type="button" class="btn btn-outline-primary btn-xs btn-select" data-uniq="<?=$r[COL_UNIQ]?>" data-desc="<?=$r[COL_PRODIKODE].' '.$r[COL_PRODINAMA].' - '.$r[COL_PRODIPT]?>">
            <i class="far fa-check"></i>
          </button>
        </td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
<script type="text/javascript">
$(document).ready(function() {
  
});
</script>
