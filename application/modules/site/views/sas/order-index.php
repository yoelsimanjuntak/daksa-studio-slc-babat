<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
#datalist tbody th, #datalist tbody td {
  vertical-align: middle;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/sas/order')?>" class="btn btn-tool btn-add-data text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH AKSES</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>AKUN</th>
                    <th>NAMA PENGGUNA</th>
                    <th>STATUS</th>
                    <th>DIBUAT PADA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-order" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">AKSES ANALISA PELUANG SNBP</span>
      </div>
      <div class="modal-body">

      </div>
      <!--<div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-outline-primary">LANJUT&nbsp;<i class="far fa-arrow-circle-right"></i></button>
      </div>-->
    </div>
  </div>
</div>
<div class="modal fade" id="modal-status" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">UBAH STATUS</span>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
        <button type="submit" class="btn btn-sm btn-outline-success">LANJUT&nbsp;<i class="far fa-arrow-circle-right"></i></button>
      </div>
    </div>
  </div>
</div>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterStatus" style="width: 200px">
    <option value="">-- SEMUA STATUS --</option>
    <option value="NEW">NEW</option>
    <option value="APPROVED">APPROVED</option>
    <option value="REJECTED">REJECTED</option>
  </select>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var orderdef = [[ 4, "desc" ]];
  var coldefs = [
    {"targets":[0,3,4], "className":'nowrap text-center'},
    {"targets":[4], "className":'nowrap dt-body-right'}
  ];
  var cols = [
    {"orderable": false,"width": "50px"},
    {"orderable": true},
    {"orderable": true},
    {"orderable": false,"width": "50px"},
    {"orderable": true,"width": "10px"}
  ];

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/sas/order-index-load')?>",
      "type": 'POST',
      "data": function(data){
        data.filterStatus = $('[name=filterStatus]', $('.filtering')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": orderdef,
    "columnDefs": coldefs,
    "columns": cols,
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-changestatus', $(row)).click(function(){
        var href = $(this).attr('href');
        $('.modal-body', $('#modal-status')).html('<p class="text-center">MEMUAT DATA<br /><i class="far fa-spinner fa-spin"></i></p>');
        $('#modal-status').modal('show');
        $('.modal-body', $('#modal-status')).load(href, function(){
          $("select", $('#modal-status')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
          $("form", $('#modal-status')).validate({
            submitHandler: function(form) {
              var btnSubmit = $('button[type=submit]', form);
              var txtSubmit = btnSubmit.html();
              btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
              btnSubmit.attr('disabled', true);

              $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success: function(res) {
                  if(res.error != 0) {
                    toastr.error(res.error);
                  } else {
                    toastr.success(res.success);
                    dt.DataTable().ajax.reload();
                    $('#modal-status').modal('hide');
                  }
                },
                error: function() {
                  toastr.error('SERVER ERROR');
                },
                complete: function() {
                  btnSubmit.html(txtSubmit);
                  btnSubmit.attr('disabled', false);
                }
              });
              return false;
            }
          });
        });
        return false;
      });

      $('[data-toggle="tooltip"]', $(row)).tooltip();
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });
  $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.filtering")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function(){
    var href = $(this).attr('href');
    $('.modal-body', $('#modal-order')).html('<p class="text-center">MEMUAT DATA<br /><i class="far fa-spinner fa-spin"></i></p>');
    $('#modal-order').modal('show');
    $('.modal-body', $('#modal-order')).load(href, function(){
      $("select", $('#modal-order')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $("form", $('#modal-order')).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', form);
          var txtSubmit = btnSubmit.html();
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          btnSubmit.attr('disabled', true);

          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                dt.DataTable().ajax.reload();
                $('#modal-order').modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
              btnSubmit.attr('disabled', false);
            }
          });
          return false;
        }
      });
      $('button[type=submit]', $('#modal-order')).click(function(){
        $("form", $('#modal-order')).submit();
        return false;
      });
    });
    return false;
  });

  $('button[type=submit]', $('#modal-order')).click(function(){
    $("form", $('#modal-order')).submit();
    return false;
  });

  $('button[type=submit]', $('#modal-status')).click(function(){
    $("form", $('#modal-status')).submit();
    return false;
  });
});
</script>
