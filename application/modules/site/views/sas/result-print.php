<?php
$rdet = $this->db
->select('sas_tformdetail.*, sas_mmapel.MapelNama')
->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
->where(COL_IDFORM, $data[COL_UNIQ])
->get(TBL_SAS_TFORMDETAIL)
->result_array();

$rdetsum = $this->db
->select('AVG(sas_tformdetail.NumNR) as AvgNilai')
->where(COL_IDFORM, $data[COL_UNIQ])
->get(TBL_SAS_TFORMDETAIL)
->row_array();

$rdetmin = $this->db
->select('sas_tformdetail.*, sas_mmapel.MapelNama')
->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
->where(TBL_SAS_TFORMDETAIL.'.'.COL_IDFORM, $data[COL_UNIQ])
->order_by(TBL_SAS_TFORMDETAIL.'.'.COL_NUMNR,'asc')
->get(TBL_SAS_TFORMDETAIL)
->row_array();

$rdetmax = $this->db
->select('sas_tformdetail.*, sas_mmapel.MapelNama')
->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
->where(TBL_SAS_TFORMDETAIL.'.'.COL_IDFORM, $data[COL_UNIQ])
->order_by(TBL_SAS_TFORMDETAIL.'.'.COL_NUMNR,'desc')
->get(TBL_SAS_TFORMDETAIL)
->row_array();

$numNR = !empty($rdetsum)?$rdetsum['AvgNilai']:0;
$numNilaiPerc = 0;
$numAkreditasi = 0;
$numDomisiliPT1 = 0;
$numDomisiliPT2 = 0;
$numAlumni = 0;
$numRankSekolah = 0;
$numRankSiswa = 0;
$numMapelPendukung = 0;
$numPiagam = 0;
$numPortfolio = 0;
$txtPeluang = '';
$numGrade = 0;
$numLevel = 0;

$arrResult = array();

if($data[COL_FORMPORTOFOLIO]=='ADA') {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'NILAI_WITH_PORTOFOLIO')
  ->where(COL_MATRANGEFROM.'<=',$numNR)
  ->where(COL_MATRANGETO.'>=',$numNR)
  ->order_by(COL_MATRANGEFROM,'desc')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();

  $numNilaiPerc = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numNilaiPerc;
} else if($data[COL_FORMPORTOFOLIO]=='TIDAK') {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'NILAI_NO_PORTOFOLIO')
  ->where(COL_MATRANGEFROM.'<=',$numNR)
  ->where(COL_MATRANGETO.'>=',$numNR)
  ->order_by(COL_MATRANGEFROM,'desc')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();

  $numNilaiPerc = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numNilaiPerc;
}

$rmatriks = $this->db
->where(COL_MATNAME,'AKREDITASI')
->where(COL_MATVAL,$data[COL_FORMAKREDITAS])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numAkreditasi = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numAkreditasi;

if($data[COL_IDPROVINSI]==$data[COL_IDPROVINSIPT1]) {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN1')
  ->where(COL_MATVAL,'YA')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT1 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT1;
} else {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN1')
  ->where(COL_MATVAL,'TIDAK')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT1 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT1;
}

if($data[COL_IDPROVINSI]==$data[COL_IDPROVINSIPT2]) {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN2')
  ->where(COL_MATVAL,'YA')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT2 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT2;
} else {
  $rmatriks = $this->db
  ->where(COL_MATNAME,'DOMISILI_PTN2')
  ->where(COL_MATVAL,'TIDAK')
  ->get(TBL_SAS_MMATRIKS)
  ->row_array();
  $numDomisiliPT2 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
  $arrResult[] = $numDomisiliPT2;
}

$rmatriks = $this->db
->where(COL_MATNAME,'ALUMNI_SNBP')
->where(COL_MATVAL,$data[COL_FORMNUMALUMNI])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numAlumni = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numAlumni;

$rmatriks = $this->db
->where(COL_MATNAME,'RANKING_SEKOLAH')
->where(COL_MATVAL,$data[COL_FORMRANKSEKOLAH])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numRankSekolah = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numRankSekolah;

$rmatriks = $this->db
->where(COL_MATNAME,'RANK_PARALEL')
->where(COL_MATVAL,$data[COL_FORMRANKPARALEL])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numRankSiswa = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numRankSiswa;

$rmatriks = $this->db
->where(COL_MATNAME,'MAPEL_PENDUKUNG')
->where(COL_MATVAL,$data[COL_FORMMAPELKESESUAIAN])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numMapelPendukung = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numMapelPendukung;

$rmatriks = $this->db
->where(COL_MATNAME,'PIAGAM')
->where(COL_MATVAL,$data[COL_FORMPIAGAM])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numPiagam = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numPiagam;

$rmatriks = $this->db
->where(COL_MATNAME,'PORTOFOLIO')
->where(COL_MATVAL,$data[COL_FORMPORTOFOLIO])
->get(TBL_SAS_MMATRIKS)
->row_array();
$numPortfolio = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
$arrResult[] = $numPortfolio;

$rmatriks = $this->db
->where(COL_MATNAME,'PELUANG')
->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
->where(COL_MATRANGETO.'>=',array_sum($arrResult))
->order_by(COL_MATRANGEFROM,'desc')
->get(TBL_SAS_MMATRIKS)
->row_array();
$txtPeluang = !empty($rmatriks)?$rmatriks[COL_MATVAL]:'';

$rmatriksGrade = $this->db
->where(COL_MATNAME,'ANALYSIS_GRADE')
->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
->where(COL_MATRANGETO.'>=',array_sum($arrResult))
->order_by(COL_MATRANGEFROM,'desc')
->get(TBL_SAS_MMATRIKS)
->row_array();
$numGrade = !empty($rmatriksGrade)?$rmatriksGrade[COL_MATPOINT]:0;

$rmatriksLevel = $this->db
->where(COL_MATNAME,'ANALYSIS_LEVEL')
->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
->where(COL_MATRANGETO.'>=',array_sum($arrResult))
->order_by(COL_MATRANGEFROM,'desc')
->get(TBL_SAS_MMATRIKS)
->row_array();
$numLevel = !empty($rmatriksLevel)?$rmatriksLevel[COL_MATPOINT]:0;

$rptn = $this->db
->where(COL_PRODITIPE, $data[COL_FORMPROGRAM])
->where(COL_NUMNILAI." >= ".$rmatriksGrade[COL_MATRANGEFROM])
->where(COL_NUMNILAI." <= ".$rmatriksGrade[COL_MATRANGETO])
->order_by(COL_NUMNILAI,'desc')
->limit(10)
->get(TBL_SAS_MPRODI)
->result_array();

$rptn1 = $this->db
->where(COL_PRODINAMA, $data['ProdiNama1'])
->where(COL_PRODITIPE, $data[COL_FORMPROGRAM])
->where(COL_NUMNILAI." >= ".$rmatriksGrade[COL_MATRANGEFROM])
->where(COL_NUMNILAI." <= ".$rmatriksGrade[COL_MATRANGETO])
->order_by(COL_NUMNILAI,'desc')
->limit(5)
->get(TBL_SAS_MPRODI)
->result_array();

$rptn2 = $this->db
->where(COL_PRODINAMA, $data['ProdiNama2'])
->where(COL_PRODITIPE, $data[COL_FORMPROGRAM])
->where(COL_NUMNILAI." >= ".$rmatriksGrade[COL_MATRANGEFROM])
->where(COL_NUMNILAI." <= ".$rmatriksGrade[COL_MATRANGETO])
->order_by(COL_NUMNILAI,'desc')
->limit(5)
->get(TBL_SAS_MPRODI)
->result_array();
?>
<html>
<head>
  <title><?=$this->setting_web_name.' - '.$title?></title>
  <style type="text/css">
  body {
    font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
    padding-top: 10px !important;
  }
  th, td {
    padding: 5px;
  }
  table {
    width: 100%;
    border-collapse: collapse;
  }
  table, th, td {
    /*border: 1px solid black !important;*/
  }

  td.is-invalid {
    color: red;
    font-weight: bold;
  }
  td.is-valid {
    color: green;
    font-weight: bold;
  }
  .table-head {
    border: 0.25px solid #000;
    margin-bottom: 15px !important;
  }
  td.cell-sm {
    width: 15px !important;
  }
  .font-weight-bold {
    font-weight: bold !important;
  }
  .text-right {
    text-align: right !important;
  }
  .text-center {
    text-align: center !important;
  }
  .v-align-top {
    vertical-align: top !important;
  }
  .nowrap {
    white-space: nowrap !important;
  }
  .w-100 {
    width: 100% !important;
  }
  .mt-5 {
    margin-top: .5rem !important;
  }
  .mt-b {
    margin-bottom: .5rem !important;
  }
  .text-green {
    color: green;
  }
  .text-red {
    color: red;
  }
  td.text-small {
    font-size: 10pt !important
  }
  .bg-lime {
    background-color: #01ff70 !important
  }
  </style>
</head>
<body>
  <h4>DATA SISWA</h4>
  <table class="table-head table-sm" width="100%">
    <tr>
      <td class="cell-sm nowrap text-small">NAMA LENGKAP</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMNAMA]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">JENIS KELAMIN</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMJK]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">ASAL SEKOLAH</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMASALSEKOLAH]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">DOMISILI</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data['Provinsi']?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">AKREDITAS SEKOLAH</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMAKREDITAS]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">JURUSAN SEKOLAH</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMJURUSAN]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">PEMINATAN</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMPROGRAM]?></strong></td>
    </tr>
  </table>

  <table class="table-head table-sm" width="100%">
    <tr>
      <td class="cell-sm nowrap text-small">DOMISILI PTN 1</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data['ProvinsiPTN1']?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">DOMISILI PTN 2</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data['ProvinsiPTN2']?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">ALUMNI 1 TAHUN TERAKHIR</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMNUMALUMNI]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">RANKING SEKOLAH</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMRANKSEKOLAH]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">RANKING PARALEL</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMRANKPARALEL]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">KESESUAIAN MAPEL PENDUKUNG</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMMAPELKESESUAIAN]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">SERTIFIKAT / PIAGAM</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMPIAGAM]?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">MEMILIKI PORTOFOLIO</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data[COL_FORMPORTOFOLIO]?></strong></td>
    </tr>
  </table>

  <h4>DATA NILAI RAPOR</h4>
  <table style="width: 100%" border="1">
    <thead>
      <tr>
        <th>No.</th>
        <th>Mata Pelajaran</th>
        <th>Nilai</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      foreach ($rdet as $r) {
        ?>
        <tr>
          <td class="cell-sm nowrap text-small text-center" style="width: 10px !important; white-space: nowrap"><?=$no?></td>
          <td class="cell-sm nowrap text-small"><?=$r[COL_MAPELNAMA]?></td>
          <td class="cell-sm nowrap text-small text-center" style="text-align: center"><?=$r[COL_NUMNR]?></td>
        </tr>
        <?php
        $no++;
      }
      ?>
    </tbody>
    <thead>
      <tr>
        <th class="text-small" colspan="2" style="text-align: center">RATA-RATA</th>
        <th class="text-small" style="text-align: center"><?=number_format($numNR, 2)?></th>
      </tr>
    </thead>
  </table>

  <h4>PILIHAN PTN 1</h4>
  <table class="table-head table-sm" width="100%">
    <tr>
      <td class="cell-sm nowrap text-small">PRODI</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data['ProdiKode1']?> - <?=$data['ProdiNama1']?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">PERGURUAN TINGGI</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data['ProdiPT1']?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">PREDIKSI NILAI</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=number_format($data['ProdiNilai1'], 2)?></strong></td>
    </tr>
  </table>

  <h4>PILIHAN PTN 2</h4>
  <table class="table-head table-sm" width="100%">
    <tr>
      <td class="cell-sm nowrap text-small">PRODI</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data['ProdiKode2']?> - <?=$data['ProdiNama2']?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">PERGURUAN TINGGI</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=$data['ProdiPT2']?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">PREDIKSI NILAI</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=number_format($data['ProdiNilai2'], 2)?></strong></td>
    </tr>
  </table>

  <h4>HASIL ANALISIS</h4>
  <table class="table-head table-sm bg-lime" width="100%">
    <tr>
      <td class="cell-sm nowrap text-small">PERKIRAAN PENCAPAIAN</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong><?=number_format(array_sum($arrResult))?> - <?=$txtPeluang?></strong></td>
    </tr>
    <tr>
      <td class="cell-sm nowrap text-small">PRODI YANG MENDEKATI NILAI ANDA</td>
      <td class="cell-sm nowrap text-small">:</td>
      <td class="text-small"><strong>LEVEL <?=$numLevel?></strong></td>
    </tr>
  </table>

  <h4>SARAN PTN BERDASARKAN PILIHAN</h4>
  <table style="width: 100%" border="1">
    <thead>
      <tr class="text-center">
        <th class="cell-sm text-small" style="vertical-align: middle">Kode</th>
        <th class="cell-sm text-small" style="vertical-align: middle">Prodi</th>
        <th class="cell-sm text-small" style="vertical-align: middle">PTN</th>
        <th class="cell-sm text-small" style="vertical-align: middle">Prediksi Nilai</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      foreach ($rptn1 as $r) {
        ?>
        <tr>
          <td class="cell-sm text-small text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_PRODIKODE]?></td>
          <td class="cell-sm text-small nowrap" style="white-space: nowrap"><?=$r[COL_PRODINAMA]?></td>
          <td class="cell-sm text-small nowrap" style="white-space: nowrap"><?=$r[COL_PRODIPT]?></td>
          <td class="cell-sm text-small text-center" style="width: 10px; white-space: nowrap"><?=number_format($r[COL_NUMNILAI],2)?></td>
        </tr>
        <?php
        $no++;
      }
      ?>
      <?php
      $no=1;
      foreach ($rptn2 as $r) {
        ?>
        <tr>
          <td class="cell-sm text-small text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_PRODIKODE]?></td>
          <td class="cell-sm text-small nowrap" style="white-space: nowrap"><?=$r[COL_PRODINAMA]?></td>
          <td class="cell-sm text-small nowrap" style="white-space: nowrap"><?=$r[COL_PRODIPT]?></td>
          <td class="cell-sm text-small text-center" style="width: 10px; white-space: nowrap"><?=number_format($r[COL_NUMNILAI],2)?></td>
        </tr>
        <?php
        $no++;
      }
      ?>
    </tbody>
  </table>

  <h4>SARAN PTN BERDASARKAN NILAI</h4>
  <table style="width: 100%" border="1">
    <thead>
      <tr class="text-center">
        <th class="cell-sm text-small" style="vertical-align: middle">Kode</th>
        <th class="cell-sm text-small" style="vertical-align: middle">Prodi</th>
        <th class="cell-sm text-small" style="vertical-align: middle">PTN</th>
        <th class="cell-sm text-small" style="vertical-align: middle">Prediksi Nilai</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      foreach ($rptn as $r) {
        ?>
        <tr>
          <td class="cell-sm text-small text-right" style="width: 10px; white-space: nowrap"><?=$r[COL_PRODIKODE]?></td>
          <td class="cell-sm text-small nowrap" style="white-space: nowrap"><?=$r[COL_PRODINAMA]?></td>
          <td class="cell-sm text-small nowrap" style="white-space: nowrap"><?=$r[COL_PRODIPT]?></td>
          <td class="cell-sm text-small text-center" style="width: 10px; white-space: nowrap"><?=number_format($r[COL_NUMNILAI],2)?></td>
        </tr>
        <?php
        $no++;
      }
      ?>
    </tbody>
  </table>
</body>
