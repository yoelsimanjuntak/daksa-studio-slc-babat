<?php
$ruser = GetLoggedUser();
?>
<form action="<?=current_url()?>" method="post">
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    ?>
    <div class="form-group">
      <label class="control-label">PENGGUNA</label>
      <select class="form-control" name="<?=COL_USERNAME?>" style="width: 100%" required>
        <?=GetCombobox("select * from users where RoleID=".ROLEUSER." order by Fullname", COL_USERNAME, array(COL_FULLNAME, COL_EMAIL))?>
      </select>
    </div>
    <div class="form-group">
      <label class="control-label">STATUS</label>
      <select class="form-control" name="<?=COL_ORDERSTATUS?>" style="width: 100%">
        <option value="NEW">NEW</option>
        <option value="APPROVED">APPROVED</option>
        <option value="REJECTED">REJECTED</option>
      </select>
    </div>
    <?php
  } else {
    ?>
    <div class="form-group">
      <input type="hidden" name="<?=COL_USERNAME?>" value="<?=$ruser[COL_USERNAME]?>" />
      <p>Ajukan permintaan akses <strong>ANALISA PELUANG SNBP</strong> kepada Administrator?</p>
    </div>
    <?php
  }
  ?>
  <div class="form-group text-right mb-0 p-3" style="border-top: 1px solid #e9ecef; margin: 0 -15px !important; padding-bottom: 0 !important">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
    <button type="submit" class="btn btn-sm btn-outline-primary">LANJUTKAN&nbsp;<i class="far fa-arrow-circle-right"></i></button>
  </div>
</form>
