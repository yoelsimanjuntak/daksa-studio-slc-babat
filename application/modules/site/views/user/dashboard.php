<?php
$ruser = GetLoggedUser();
$rpackage = $this->db
->where(COL_PKGISACTIVE, 1)
->order_by(COL_PKGNAME)
->get(TBL_MTESTPACKAGE)
->result_array();

$rsess = array();
$rsess_ = array();
$rsessAll = array();
if($ruser[COL_ROLEID]==ROLEUSER) {
  $rsess = $this->db
  ->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems')
  ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
  ->where("SessTimeEnd is null")
  ->where(COL_USERNAME, $ruser[COL_USERNAME])
  ->order_by('tsession.Uniq', 'desc')
  ->get(TBL_TSESSION)
  ->row_array();
  $rsess_ = $this->db
  ->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems')
  ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
  ->where("SessTimeEnd is null")
  ->where(COL_USERNAME, $ruser[COL_USERNAME])
  ->order_by('tsession.Uniq', 'desc')
  ->get(TBL_TSESSION)
  ->result_array();

  $rsessAll = $this->db
  ->select('tsession.*, mtestpackage.PkgName, mtestpackage.PkgDesc, mtestpackage.PkgPrice, mtestpackage.PkgItems, (select sum(TestScore) from tsessiontest ts where ts.IDSession=tsession.Uniq) as SessScore')
  ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
  ->where("SessTimeEnd is not null")
  ->where(COL_USERNAME, $ruser[COL_USERNAME])
  ->order_by(COL_SESSTIMEEND, 'desc')
  ->get(TBL_TSESSION)
  ->result_array();
}

$lastScore = '-';
if(!empty($rsessAll)&&count($rsessAll)>0) {
  $rtest = $this->db
  ->query("select * from tsessiontest where TestRemarks1 is null and IDSession=".$rsessAll[0][COL_UNIQ])
  ->result_array();

  $sum=0;
  foreach($rtest as $t) {
    $rquest = $this->db
    ->query("select sum(QuestScore) as QuestScore from tsessionsheet where IDTest=".$t[COL_UNIQ])
    ->row_array();

    if($t[COL_TESTNAME]!='Pass Hand A4') {
      $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
    }
  }
  $lastScore = $sum;
}
?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $countUser = $this->db
        ->where(COL_ROLEID, ROLEUSER)
        ->count_all_results(TBL_USERS);

        $countUserActive = $this->db
        ->where(COL_ISSUSPEND, 0)
        ->where(COL_ROLEID, ROLEUSER)
        ->count_all_results(TBL_USERS);

        $countUserInactive = $this->db
        ->where(COL_ISSUSPEND, 1)
        ->where(COL_ROLEID, ROLEUSER)
        ->count_all_results(TBL_USERS);

        $countSess = $this->db
        ->count_all_results(TBL_TSESSION);

        $countSessNew = $this->db
        ->where(COL_SESSTIMEEND, null)
        ->count_all_results(TBL_TSESSION);

        $countSessEnd = $this->db
        ->where(COL_SESSTIMEEND.' != ', null)
        ->count_all_results(TBL_TSESSION);

        $countSubsActive = $this->db
        ->where(COL_SUBSDATETO.' >= ', date('Y-m-d'))
        ->count_all_results(TBL_TSUBSCRIPTION);

        $countSubsExpired = $this->db
        ->where(COL_SUBSDATETO.' < ', date('Y-m-d'))
        ->count_all_results(TBL_TSUBSCRIPTION);

        $countModActive = $this->db
        ->where(COL_MODISAKTIF, 1)
        ->count_all_results(TBL_MMODUL);

        $countModSuspended = $this->db
        ->where(COL_MODISAKTIF." != ", 1)
        ->count_all_results(TBL_MMODUL);
        ?>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($countUser)?></h3>
              <p class="mb-0">
                <strong>Pengguna</strong><br />
                <small>
                  <i class="far fa-check-circle"></i>&nbsp;AKTIF : <strong><?=number_format($countUser)?></strong><br />
                  <i class="far fa-times-circle"></i>&nbsp;SUSPENDED : <strong><?=number_format($countUserInactive)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-users"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>0</h3>
              <p class="mb-0">
                <strong>Subscription</strong><br />
                <small>
                  <i class="far fa-check-circle"></i>&nbsp;AKTIF : <strong><?=number_format($countSubsActive)?></strong><br />
                  <i class="far fa-times-circle"></i>&nbsp;BERAKHIR : <strong><?=number_format($countSubsExpired)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-book-reader"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h3><?=number_format($countSess)?></h3>
              <p class="mb-0">
                <strong>Sesi Test / Ujian</strong><br />
                <small>
                  <i class="far fa-exclamation-circle"></i>&nbsp;BARU / BERJALAN : <strong><?=number_format($countSessNew)?></strong><br />
                  <i class="far fa-check-circle"></i>&nbsp;SELESAI : <strong><?=number_format($countSessEnd)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-file-edit"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-12">
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3>0</h3>
              <p class="mb-0">
                <strong>Modul Belajar</strong><br />
                <small>
                  <i class="far fa-check-circle"></i>&nbsp;DITERBITKAN : <strong><?=number_format($countModActive)?></strong><br />
                  <i class="far fa-exclamation-circle"></i>&nbsp;DITANGGUHKAN : <strong><?=number_format($countModSuspended)?></strong>
                </small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-books"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title font-weight-bold">SESI TEST / UJIAN BERJALAN</h3>
            </div>
            <div class="card-body table-responsive">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 10px">#</th>
                      <th>PAKET</th>
                      <th>PESERTA</th>
                      <th>MULAI</th>
                      <th>TEST BERJALAN</th>
                      <th>SISA WAKTU</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/sess/index/active')?>" class="btn btn-sm btn-primary">LIHAT SEMUA&nbsp;<i class="far fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        <?php
      } else {
        $rmodul = $this->db
        ->where(COL_MODISAKTIF, 1)
        ->order_by(COL_CREATEDON, 'desc')
        ->get(TBL_MMODUL)
        ->result_array();

        $rsubs = $this->db
        ->where(COL_SUBSDATETO.' >= ', date('Y-m-d'))
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->get(TBL_TSUBSCRIPTION)
        ->row_array();

        $subRemain = 0;
        if(!empty($rsubs)) {
          $subRemain = round((strtotime($rsubs[COL_SUBSDATETO]) - time()) / (60 * 60 * 24));
        }
        ?>
        <?php
        if(empty($ruser[COL_GENDER]) || empty($ruser[COL_PHONE]) || empty($ruser[COL_DATEBIRTH])) {
          ?>
          <!--<div class="col-lg-12">
            <div class="callout callout-danger">
              <h6 class="font-weight-bold"><i class="fas fa-exclamation-circle text-danger"></i>&nbsp;PROFIL BELUM LENGKAP !</h6>
              <p class="font-italic">
                <strong><?=$ruser[COL_FULLNAME]?></strong>, profil kamu belum lengkap, nih.<br />
                Silakan lengkapi dan perbarui profil kamu sebelum kamu menikmati fitur dan layanan <?=$this->setting_web_name?> ya!
              </p>
              <p>
                <button type="button" id="btn-profil" class="btn btn-sm btn-danger">PERBARUI PROFIL&nbsp;<i class="far fa-arrow-circle-right"></i></button>
              </p>
            </div>
          </div>-->
          <?php
        }
        ?>
        <!--<div class="col-lg-6 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($subRemain>=0?$subRemain:0)?> <small class="text-sm">HARI</small></h3>
              <p class="mb-0">
                <strong>Masa Berlangganan</strong><br />
                <?=!empty($rsubs)?'<small>Berakhir pada tgl. <strong>'.date('d-m-Y', strtotime($rsubs[COL_SUBSDATETO])).'</strong></small>':'-'?>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-book-reader"></i>
            </div>
          </div>
        </div>-->
        <!--<div class="col-lg-6 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format(count($rsessAll))?></h3>
              <p class="mb-0">
                <strong>Simulasi Test / Ujian</strong><br />
                <small>Poin terakhir <strong><?=$lastScore?></strong></small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-file-edit"></i>
            </div>
          </div>
        </div>-->
        <!--<div class="col-lg-4 col-12">
          <div class="small-box bg-indigo">
            <div class="inner">
              <h3><?=count($rmodul)?></h3>
              <p class="mb-0">
                <strong>Modul Pembelajaran</strong><br />
                <small>Terakhir ditambahkan pada tgl. <?=!empty($rmodul)?'<strong>'.date('d-m-Y', strtotime($rmodul[0][COL_CREATEDON])).'</strong>':''?></small>
              </p>
            </div>
            <div class="icon">
              <i class="fas fa-books"></i>
            </div>
          </div>
        </div>-->
        <?php
        if(empty($rsess) || empty($rsubs)) {
          $txtSub = urlencode("Saya ingin mendapatkan akses modul belajar di ".$this->setting_web_desc."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
          ?>
          <div class="col-lg-6">
            <div class="card card-outline card-primary">
              <div class="card-header">
                <h6 class="card-title font-weight-bold"><i class="fas fa-phone-laptop"></i>&nbsp;&nbsp;SIMULASI CAT ONLINE</h6>
              </div>
              <div class="card-body">
                <p>Selamat datang di platform CAT <strong><?=$this->setting_web_name?></strong>. Klik tombol dibawah untuk mendapatkan berbagai pilihan paket belajar dan akses simulasi test / ujian.</p>
              </div>
              <div class="card-footer text-right">
                <a href="<?=site_url('site/user/package')?>" class="btn btn-sm btn-outline-primary">PILIH PAKET SIMULASI&nbsp;<i class="far fa-arrow-circle-right"></i></a>
              </div>
            </div>
          </div>
          <?php
        } if(!empty($rsess) && !empty($rsess[COL_SESSTIMESTART])) {
          $rpackageCurr = $this->db
          ->where(COL_UNIQ, $rsess[COL_IDPACKAGE])
          ->get(TBL_MTESTPACKAGE)
          ->row_array();
          if(!empty($rpackageCurr)) {
            $arrPkgItems = json_decode($rpackageCurr[COL_PKGITEMS]);
            ?>
            <div class="col-lg-6">
              <div class="card card-outline card-primary">
                <div class="card-header">
                  <h6 class="card-title text-primary font-weight-bold"><i class="fas fa-exclamation-circle"></i>&nbsp;&nbsp;SESI SIMULASI</h6>
                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                  </div>
                </div>
                <div class="card-body">
                  <p class="mb-0">
                    Hai, <strong><?=strtoupper($ruser[COL_FULLNAME])?></strong>!<br />
                    Kamu memiliki sesi <strong style="text-decoration: underline"><?=strtoupper($rsess[COL_PKGNAME])?></strong> yang sedang berjalan. Silakan klik tombol dibawah untuk melanjutkan sesi sebelum waktu habis.
                  </p>
                </div>
                <div class="card-footer">
                  <p class="text-right mb-0">
                    <a <?=empty($rsess[COL_SESSTIMESTART])?'id="btn-start"':''?> href="<?=site_url('site/sess/start/'.$rsess[COL_UNIQ])?>" class="btn btn-sm btn-primary"><?=!empty($rsess[COL_SESSTIMESTART])?'LANJUTKAN':'MULAI'?>&nbsp;&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                  </p>
                </div>
              </div>
            </div>
            <?php
          }
        } else if(!empty($rsess_)) {
          ?>
          <div class="col-lg-6">
            <div class="card card-outline card-primary">
              <div class="card-header">
                <h6 class="card-title text-primary font-weight-bold"><i class="fas fa-exclamation-circle"></i>&nbsp;&nbsp;SESI SIMULASI</h6>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <p class="mb-0">
                  Hai, <strong><?=strtoupper($ruser[COL_FULLNAME])?></strong>!<br />
                  Kamu memiliki sesi simulasi yang belum dikerjakan. Silakan klik tombol MULAI untuk mengerjakan sesi berikut.
                </p>
              </div>
              <div class="card-footer p-0">
                <ul class="nav nav-pills flex-column">
                  <?php
                  foreach($rsess_ as $r) {
                    ?>
                    <li class="nav-item">
                      <span class="nav-link" style="padding: 1rem 1rem !important">
                        <strong><?=$r[COL_PKGNAME]?></strong>
                        <span class="float-right text-danger">
                          <a class="btn btn-xs btn-primary btn-start" href="<?=site_url('site/sess/start/'.$r[COL_UNIQ])?>">MULAI&nbsp;&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                        </span>
                      </span>
                    </li>
                    <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
        <div class="col-lg-6">
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h6 class="card-title font-weight-bold"><i class="fas fa-chart-network"></i>&nbsp;&nbsp;ANALISA PELUANG SNBP</h6>
            </div>
            <div class="card-body">
              <p><strong>SNBP ANALYSIS SYSTEM (SAS)</strong> adalah metode yang berguna dalam memberikan gambaran dan referensi dalam penentuan Program Studi dan PTN pilihan jalur SNBP.</p>
            </div>
            <div class="card-footer text-right">
              <a id="btn-snbp" href="<?=site_url('site/sas/order')?>" class="btn btn-sm btn-outline-primary">ANALISA PELUANG SNBP&nbsp;<i class="far fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        <div class="col-lg-12">
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h6 class="card-title font-weight-bold text-primary"><i class="fas fa-history"></i>&nbsp;&nbsp;RIWAYAT TEST / UJIAN</h6>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body p-0">
              <div class="table-responsive">
                <table class="table table-striped mb-0 text-sm" style="max-width: 100%">
                  <thead>
                    <tr>
                      <th>NAMA PAKET</th>
                      <th>MULAI</th>
                      <th>SELESAI</th>
                      <th class="text-center">POIN</th>
                      <th class="text-center">#</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    if(!empty($rsessAll)) {
                      foreach($rsessAll as $s) {
                        $rtest = $this->db
                        ->query("select * from tsessiontest where TestRemarks1 is null and IDSession=".$s[COL_UNIQ])
                        ->result_array();

                        $sum=0;
                        foreach($rtest as $t) {
                          $rquest = $this->db
                          ->query("select sum(QuestScore) as QuestScore from tsessionsheet where IDTest=".$t[COL_UNIQ])
                          ->row_array();

                          if($t[COL_TESTNAME]!='Pass Hand A4') {
                            $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
                          }
                        }
                        ?>
                        <tr>
                          <td style="vertical-align: middle"><?=strtoupper($s[COL_PKGNAME])?></td>
                          <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><?=date('H:i:s', strtotime($s[COL_SESSTIMESTART]))?></td>
                          <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><?=date('H:i:s', strtotime($s[COL_SESSTIMEEND]))?></td>
                          <td style="vertical-align: middle; white-space: nowrap" class="text-center"><strong><?=number_format($sum)?></strong></td>
                          <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-center">
                            <a href="<?=site_url('site/sess/result/'.$s[COL_UNIQ])?>" class="btn btn-primary btn-xs"><i class="far fa-info-circle"></i>&nbsp;HASIL</a>
                          </td>
                        </tr>
                        <?php
                      }
                    } else {
                      ?>
                      <tr>
                        <td colspan="5" class="text-center font-italic">BELUM ADA DATA TERSEDIA</td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
            <!--<div class="card-footer">
              <p class="text-right mb-0">
                <a href="#" class="btn btn-sm btn-success">LIHAT SEMUA&nbsp;&nbsp;<i class="far fa-arrow-circle-right"></i></a>
              </p>
            </div>-->
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <select class="form-control" name="filterPackage" style="width: 200px">
    <?=GetCombobox("select * from mtestpackage order by PkgName", COL_UNIQ, COL_PKGNAME, null, true, false, '-- SEMUA PAKET --')?>
  </select>
</div>
<div class="modal fade" id="modal-test" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">PAKET SIMULASI</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
          <?php
          if(empty($rpackage)) {
            ?>
            <p class="text-muted">Maaf, belum ada paket tersedia untuk saat ini.</p>
            <?php
          } else {
            ?>
            <p class="text-muted font-italic">Silakan pilih paket simulasi dibawah ini:</p>
            <?php
            foreach($rpackage as $p) {
              $txt = urlencode("Saya ingin mendapatkan akses simulasi *".strtoupper($p[COL_PKGNAME])."* di ".$this->setting_web_desc."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
              ?>
              <div class="callout callout-info">
                <h6 class="font-weight-bold"><?=strtoupper($p[COL_PKGNAME])?><span class="badge badge-success float-right">Rp. <?=number_format($p[COL_PKGPRICE])?></span></h6>
                <p class="text-sm text-muted mb-2"><?=$p[COL_PKGDESC]?></p>
                <p>
                  <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=<?=$txt?>" target="_blank" class="btn btn-sm btn-info" style="color: #fff; text-decoration: none">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                </p>
              </div>
              <?php
            }
          }
          ?>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-start" role="dialog">
  <div class="modal-dialog modal-sm modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">MULAI SESI</h5>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer d-none">
        <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;TUTUP</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-snbp" role="dialog">
  <div class="modal-dialog modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h6 class="modal-title font-weight-bold">ANALISA PELUANG SNBP</h6>
      </div>
      <div class="modal-body">

      </div>
      <!--<div class="modal-footer"></div>-->
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  <?php
  if($ruser[COL_ROLEID]==ROLEADMIN) {
    ?>
    var dt = $('#datalist').dataTable({
      "autoWidth" : false,
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?=site_url('site/sess/index-load/active')?>",
        "type": 'POST',
        "data": function(data){
          data.filterPackage = $('[name=filterPackage]', $('.filtering')).val();
         }
      },
      "iDisplayLength": 100,
      "oLanguage": {
        "sSearch": "FILTER "
      },
      "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
      "order": [[ 3, "desc" ]],
      "columnDefs": [
        {"targets":[0], "className":'nowrap text-center'},
        {"targets":[3, 5], "className":'nowrap dt-body-right'}
      ],
      "columns": [
        {"orderable": false,"width": "50px"},
        {"orderable": true},
        {"orderable": true},
        {"orderable": true},
        {"orderable": true},
        {"orderable": false,"width": "10px"}
      ],
      "createdRow": function(row, data, dataIndex) {
        $('.btn-action', $(row)).click(function() {
          var url = $(this).attr('href');
          if(confirm('Apakah anda yakin?')) {
            $.get(url, function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
              }
            }, "json").done(function() {
              dt.DataTable().ajax.reload();
            }).fail(function() {
              toastr.error('SERVER ERROR');
            });
          }
          return false;
        });
        $('[data-toggle="tooltip"]', $(row)).tooltip();
      },
      "initComplete": function(settings, json) {
        $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
      }
    });
    $("div.filtering").html($('#dom-filter').html()).addClass('d-inline-block ml-2');
    $('.btn-refresh-data').click(function() {
      dt.DataTable().ajax.reload();
    });
    $('input,select', $("div.filtering")).change(function() {
      dt.DataTable().ajax.reload();
    });
    setInterval(function(){
      dt.DataTable().ajax.reload();
    }, 5000);
    <?php
  }
  ?>
  $('#btn-profil').click(function(){
    $('#btn-changeprofile').click();
  });
  $('.btn-start').click(function() {
    var href = $(this).attr('href');
    $('.modal-body', $('#modal-start')).html('<p class="text-center">MEMUAT DATA<br /><i class="far fa-spinner fa-spin"></i></p>');
    $('.modal-footer', $('#modal-start')).addClass('d-none');
    $('#modal-start').modal({backdrop: 'static', keyboard: false});
    $.get(href, function(data) {
      data=JSON.parse(data);

      if(data.error) {
        $('.modal-body', $('#modal-start')).html('<p class="text-danger mb-0">'+data.error+'</p>');
        $('.modal-footer', $('#modal-start')).removeClass('d-none');
        return false;
      }

      if(!data.redirect) {
        $('.modal-body', $('#modal-start')).html('<p class="text-danger mb-0">Mohon maaf, telah terjadi kesalahan pada sistem. Silakan coba beberapa saat lagi.</p>');
        $('.modal-footer', $('#modal-start')).removeClass('d-none');
        return false;
      }

      $('.modal-body', $('#modal-start')).html('<p class="text-center">MENGALIHKAN<br /><i class="far fa-spinner fa-spin"></i></p>');
      setTimeout(function(){
        $('.modal-body', $('#modal-start')).html('');
        $('#modal-start').modal('hide');
        window.location.href = data.redirect;
      }, 3000);
    });
    return false;
  });

  $('#btn-snbp').click(function() {
    var href = $(this).attr('href');
    $('.modal-body', $('#modal-snbp')).html('<p class="text-center">MEMUAT DATA<br /><i class="far fa-spinner fa-spin"></i></p>');
    $('#modal-snbp').modal('show');
    $('.modal-body', $('#modal-snbp')).load(href, function(){
      $("select", $('#modal-snbp')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $("form", $('#modal-snbp')).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', form);
          var txtSubmit = btnSubmit.html();
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          btnSubmit.attr('disabled', true);

          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                setTimeout(function(){
                  location.reload();
                }, 1000);
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
              btnSubmit.attr('disabled', false);
            }
          });
          return false;
        }
      });
      $('button[type=submit]', $('#modal-snbp')).click(function(){
        $("form", $('#modal-snbp')).submit();
        return false;
      });
    });
    return false;
  });


});
</script>
