<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row align-items-stretch">
      <?php
      if(!empty($data)) {
        ?>
        <div class="col-12 col-sm-12 d-flex align-items-stretch">
          <div class="card card-outline card-primary w-100">
            <div class="card-body p-0 table-responsive">
              <table class="table table-striped" style="max-width: 100%">
                <thead>
                  <tr>
                    <th>NAMA PAKET</th>
                    <th class="d-none d-sm-table-cell">MULAI</th>
                    <th class="d-none d-sm-table-cell">SELESAI</th>
                    <th class="text-center">POIN</th>
                    <th class="text-center">#</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($data as $s) {
                    $rtest = $this->db
                    ->query("select * from tsessiontest where TestRemarks1 is null and IDSession=".$s[COL_UNIQ])
                    ->result_array();

                    $sum=0;
                    foreach($rtest as $t) {
                      $rquest = $this->db
                      ->query("select sum(QuestScore) as QuestScore from tsessionsheet where IDTest=".$t[COL_UNIQ])
                      ->row_array();

                      if($t[COL_TESTNAME]!='Pass Hand A4') {
                        $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
                      }
                    }
                    ?>
                    <tr>
                      <td style="vertical-align: middle"><?=strtoupper($s[COL_PKGNAME])?></td>
                      <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="d-none d-sm-table-cell text-right"><?=date('H:i:s', strtotime($s[COL_SESSTIMESTART]))?></td>
                      <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="d-none d-sm-table-cell text-right"><?=date('H:i:s', strtotime($s[COL_SESSTIMEEND]))?></td>
                      <td style="vertical-align: middle; white-space: nowrap" class="text-center"><strong><?=number_format($sum)?></strong></td>
                      <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-center">
                        <a href="<?=site_url('site/sess/result/'.$s[COL_UNIQ])?>" class="btn btn-primary btn-xs"><i class="far fa-info-circle"></i>&nbsp;HASIL</a>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                Maaf, belum ada data tersedia saat ini.
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
