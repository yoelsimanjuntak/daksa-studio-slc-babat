
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?=!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name?></title>
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?=base_url()?>assets/js/moment.js"></script>
  <script src="<?=base_url()?>assets/js/countdown.js"></script>
  <script src="<?=base_url()?>assets/js/moment-countdown.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>

  <style>
  .no-js #loader { display: none;  }
  .js #loader { display: block; position: absolute; left: 100px; top: 0; }
  .se-pre-con {
      position: fixed;
      left: 0px;
      top: 0px;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background: url(<?=base_url().$this->setting_web_preloader?>) center no-repeat #fff;
  }

  @media (max-width: 767px) {
    .sidebar-toggle {
      font-size: 3vw !important;
    }
  }

  .form-group .control-label {
      text-align: right;
      line-height: 2;
  }
  .nowrap {
    white-space: nowrap;
  }
  .va-middle {
    vertical-align: middle !important;
  }
  .border-0 td {
    border: 0!important;
  }
  td.dt-body-right {
    text-align: right !important;
  }
  .sidebar-dark-orange .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-orange .nav-sidebar>.nav-item>.nav-link.active {
    color: #fff !important;
  }
  div.tooltip-inner {
    max-width: 350px !important;
    text-align: left !important;
  }
  .table-condensed td, .table-condensed th {
    padding: .5rem !important;
  }
  span.badge-opt {
    font-size: 16px !important;
    font-weight: bold !important;
    position: absolute !important;
    left: -10px !important;
    top: -7px !important;
  }
  span.badge-weight {
    font-size: 16px !important;
    font-weight: bold !important;
    position: absolute !important;
    left: -10px !important;
    top: 20px !important;
  }

  .div-quest img {
    max-width: 100% !important;
  }

  .btn-outline-success:hover {
    color: #fff !important;
    background-color: #28a745 !important;
    border-color: #28a745;
  }
  </style>
  <script>
  // Wait for window load
  function startTime() {
    var today = new Date();
    $('#datetime').html(moment(today).format('DD')+' '+moment(today).format('MMM')+' '+moment(today).format('Y')+' <span class="text-muted font-weight-bold">'+moment(today).format('hh')+':'+moment(today).format('mm')+':'+moment(today).format('ss')+'</span>');
    var t = setTimeout(startTime, 1000);
  }
  $(document).ready(function() {
    startTime();
  });
  $(window).load(function() {
      $(".se-pre-con").fadeOut("slow");
  });
  </script>
</head>
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_FULLNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'icon-user.jpg';
?>
<body class="hold-transition layout-top-nav">
  <div class="se-pre-con"></div>
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
      <div class="container">
        <a href="<?=site_url('site/user/dashboard')?>" class="navbar-brand">
          <img src="<?=base_url().$this->setting_web_logo?>" alt="Logo" class="brand-image">
          <span class="brand-text font-weight-light"><?=$this->setting_web_name?></span>
        </a>
        <ul class="navbar-nav">
          <li class="nav-item dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link"><i class="fas fa-bars"></i></a>
            <ul class="dropdown-menu border-0 shadow">
              <li><a href="<?=site_url('site/user/dashboard')?>" class="dropdown-item">Dashboard</a></li>
              <li><a href="javascript: window.close()" class="dropdown-item">Keluar</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
    <div class="content-wrapper">
      <?=$content?>
    </div>
    <footer class="main-footer text-center">
      <strong>Copyright &copy; <?=$this->setting_org_name?>.</strong> Strongly developed by <strong>Partopi Tao</strong>.
    </footer>
  </div>
</body>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/fastclick/fastclick.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/sparklines/sparkline.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
<script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/sweetalert.min.js"></script>
<script>
$(document).ready(function(){
    $('.dropdown-menu textarea,.dropdown-menu input, .dropdown-menu label').click(function(e) {
        e.stopPropagation();
    });
    $(document).on('keypress','.angka',function(e){
      if((e.which <= 57 && e.which >= 48) || (e.keyCode >= 37 && e.keyCode <= 40) || e.keyCode==9 || e.which==43 || e.which==44 || e.which==45 || e.which==46 || e.keyCode==8){
          return true;
      }else{
          return false;
      }
    });
    $(document).on('blur','.uang',function(){
        $(this).val(desimal($(this).val(),0));
    }).on('focus','.uang',function(){
        $(this).val(toNum($(this).val()));
    });
    $(".uang").trigger("blur");
    $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
    $( ".alert-dismissible" ).fadeOut(3000, function() {
        // Animation complete.
    });
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $(".money").number(true, 2, '.', ',');
    $(".uang").number(true, 0, '.', ',');
});
</script>
</html>
