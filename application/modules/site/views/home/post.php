<div class="breadcrumbs overlay" style="padding: 75px 0px !important; background:url(<?=base_url()?>assets/themes/mediplus-lite/img/section-bg-sakip.jpeg) !important">
  <div class="container">
    <div class="bread-inner">
      <div class="row">
        <div class="col-12">
          <h2><?=strtoupper($title)?></h2>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="blog section" id="blog" style="background: #f9f9f9 !important; padding-top: 0 !important">
  <div class="container">
    <?php
    if($rcat[COL_ISDOCUMENTONLY]) {
      ?>
      <div class="row">
        <div class="col-lg-12 col-12">
          <div class="main-sidebar">
            <div class="single-widget search">
              <div class="row">
                <div class="col-sm-12 col-12">
                  <table id="datalist" class="table table-bordered"></table>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <?php
    } else {
      ?>
      <div class="row">
        <div class="col-lg-12 col-12">
          <div class="main-sidebar">
            <div class="single-widget search">
              <div class="form">
                <form action="<?=current_url()?>" method="GET">
                  <input type="text" name="keyword" placeholder="Pencarian" <?=!empty($_GET['keyword'])?'value="'.$_GET['keyword'].'"':''?> />
                  <button type="submit" class="button"><i class="fa fa-search"></i></button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-12">
          <div class="single-main">
            <?php
            if(!empty($res)) {
              ?>
              <div class="row">
                <?php
                $n=0;
                foreach($res as $r) {
                  $n++;
                  $strippedcontent = strip_tags($r[COL_POSTCONTENT]);
                  $img = $this->db->where(COL_POSTID, $r[COL_POSTID])->get(TBL__POSTIMAGES)->row_array();
                  ?>
                  <div class="col-lg-4 col-md-6 col-12" style="padding-top: 20px!important">
                    <div class="single-news">
                      <div class="news-head">
                        <div style="
                        height: 250px;
                        width: 100%;
                        background-image: url('<?=!empty($img)?MY_UPLOADURL.$img[COL_FILENAME]:MY_IMAGEURL.'no-image.png'?>');
                        background-size: cover;
                        background-repeat: no-repeat;
                        background-position: center;
                        ">
                        </div>
                      </div>
                      <div class="news-body">
                        <div class="news-content">
                          <div class="date"><?=date('d-m-Y', strtotime($r[COL_CREATEDON]))?></div>
                          <h2>
                            <a href="<?=site_url('site/home/page/'.$r[COL_POSTSLUG])?>"><h5 class="text-red"><?=$r[COL_POSTTITLE]?></h5></a>
                          </h2>
                          <p class="text">
                            <?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
              <div class="row" style="margin-top: 20px">
                <div class="col-lg-12 col-md-12 col-12">
                  <div class="form-group">
                    <?php
                    if($start==0) {
                      ?>
                      <button type="submit" class="btn" style="background: #323232 !important" disabled>
                        <i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA
                      </button>
                      <?php
                    } else {
                      ?>
                      <a href="?start=<?=$start-9?>" class="btn" style="background: #323232 !important; color: #fff"><i class="far fa-chevron-left"></i>&nbsp;SEBELUMNYA</a>
                      <?php
                    }

                    if($start+9>$count) {
                      ?>
                      <button type="submit" class="btn" style="background: #E71414 !important" disabled>
                        SELANJUTNYA <i class="far fa-chevron-right"></i>
                      </button>
                      <?php
                    } else {
                      ?>
                      <a href="?start=<?=$start+9?>" class="btn" style="background: #E71414 !important; color: #fff">SELANJUTNYA&nbsp;<i class="far fa-chevron-right"></i></a>
                      <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
              <?php
            } else {
              ?>
              <div class="row">
                <div class="col-lg-12 col-md-12 col-12" style="padding-top: 20px!important">
                  <div class="single-news">
                    <div class="news-body">
                      <div class="news-content">
                        <p style="text-align: center; font-style: italic">DATA <?=!empty($_GET['keyword'])?'DENGAN KATA KUNCI "<strong style="text-transform: none !important">'.$_GET['keyword'].'</strong>"':''?> TIDAK DITEMUKAN</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  <?php
  if($rcat[COL_ISDOCUMENTONLY]) {
    $arrData = array();
    foreach ($res as $r) {
      $rfiles = $this->db
      ->where(COL_POSTID, $r[COL_POSTID])
      ->get(TBL__POSTIMAGES)
      ->result_array();
      $htmlFile = '<ul>';
      foreach($rfiles as $f) {
        $fsize = filesize(MY_UPLOADPATH.$f[COL_FILENAME]);

        $htmlFile .='<li style="margin-bottom: 5px !important"><a href="'.MY_UPLOADURL.$f[COL_FILENAME].'" target="_blank" style="font-size: 12px; color: #E71414; text-transform: none"><i class="far fa-download"></i>&nbsp;DOWNLOAD&nbsp;('.strtoupper(human_filesize($fsize, 0)).')</a></li>';
      }
      $htmlFile .= '<ul>';
      $arrData[] = array(
        date('Y-m-d', strtotime($r[COL_CREATEDON])),
        $r[COL_POSTTITLE],
        $htmlFile
      );
    }
    ?>
    var dataTable = $('#datalist').dataTable({
      "autoWidth":false,
      //"bJQueryUI": true,
      "aaData": <?=json_encode($arrData)?>,
      //"scrollY" : '40vh',
      //"scrollX": "120%",
      "iDisplayLength": 50,
      //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      "dom":"R<'row'<'col-sm-12'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12'p>>",
      "order": [[ 0, "desc" ]],
      "columnDefs": [
        {"targets":[0], "className":'nowrap dt-body-right'},
        {"targets":[2], "className":'nowrap'}
      ],
      "aoColumns": [
          {"sTitle": "TANGGAL", "sWidth":"20px"},
          {"sTitle": "DOKUMEN","bSortable":false},
          {"sTitle": "FILE","bSortable":false}
      ]
    });
    <?php
  }
  ?>
});
</script>
