<?php
class Subscription extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      if (!$this->input->is_ajax_request()) {
        redirect('site/user/login');
      } else {
        ShowJsonError('HARAP LOGIN TERLEBIH DAHULU!');
        exit();
      }
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      show_error('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }
    $data['title'] = 'Subscription';
    $this->template->load('adminlte', 'sub/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_USERNAME,COL_SUBSDATEFROM,COL_SUBSDATETO,null,null,COL_CREATEDON);
    $cols = array(COL_USERNAME);

    $queryAll = $this->db
    ->get(TBL_TSUBSCRIPTION);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($filterStatus)) {
      if($filterStatus==1) {
        $this->db->where(COL_SUBSDATETO." >= ", date('Y-m-d'));
      } else {
        $this->db->where(COL_SUBSDATETO." < ", date('Y-m-d'));
      }
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tsubscription.*, users.Fullname, users.Email, users.Phone')
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSUBSCRIPTION.".".COL_USERNAME,"left")
    ->get_compiled_select(TBL_TSUBSCRIPTION, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/subscription/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i>&nbsp;HAPUS</a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_USERNAME],
        date('Y-m-d', strtotime($r[COL_SUBSDATEFROM])),
        date('Y-m-d', strtotime($r[COL_SUBSDATETO])),
        number_format($r[COL_SUBSDUR]),
        $r[COL_SUBSTERM],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      show_error('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }

    if(!empty($_POST) && $this->input->is_ajax_request()) {
      $rparticipant = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_USERNAME))
      ->get(TBL_USERS)
      ->row_array();

      if(empty($rparticipant)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }
      if($rparticipant[COL_ISSUSPEND]==1) {
        ShowJsonError('Mohon maaf, akun pengguna an. <strong>'.$rparticipant[COL_FULLNAME].'</strong> sedang berstatus SUSPEND. Silakan ubah status akun terlebih dahulu.');
        exit();
      }

      $subDur = toNum($this->input->post(COL_SUBSDUR));
      $subTerm = $this->input->post(COL_SUBSTERM);
      $subStart = strtotime($this->input->post(COL_SUBSDATEFROM));
      $subEnd = strtotime('+'.$subDur.' '.($subTerm.($subDur>1?'s':'')), $subStart);

      $dat = array(
        COL_USERNAME=>$this->input->post(COL_USERNAME),
        COL_SUBSDATEFROM=>date('Y-m-d H:i:s', $subStart),
        COL_SUBSDATETO=>date('Y-m-d H:i:s', $subEnd),
        COL_SUBSDUR=>$subDur,
        COL_SUBSTERM=>$subTerm,
        COL_SUBSREMARKS=>$this->input->post(COL_SUBSREMARKS),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_TSUBSCRIPTION, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Penambahan Subscription an. <strong>'.$rparticipant[COL_FULLNAME].'</strong> berhasil.');
      exit();
    } else {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      ShowJsonError('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TSUBSCRIPTION);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function modul() {
    $ruser = GetLoggedUser();
    $rsubs = $this->db
    ->where(COL_SUBSDATETO.' >= ', date('Y-m-d'))
    ->where(COL_USERNAME, $ruser[COL_USERNAME])
    ->get(TBL_TSUBSCRIPTION)
    ->row_array();

    $data['title'] = 'Modul Belajar';
    $data['subs'] = $rsubs;
    $this->template->load('adminlte', 'sub/index-modul', $data);
  }

  public function modul_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterKategori = !empty($_POST['filterKategori'])?$_POST['filterKategori']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_KATEGORI,COL_MODTITLE,COL_MODDESC,COL_CREATEDON);
    $cols = array(COL_KATEGORI,COL_MODTITLE,COL_MODDESC);

    $queryAll = $this->db
    ->where(COL_MODISAKTIF,1)
    ->get(TBL_MMODUL);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($filterKategori)) {
      $this->db->where(COL_IDKATEGORI, $filterKategori);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('mmodul.*, mkategori.Kategori')
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MMODUL.".".COL_IDKATEGORI,"left")
    ->where(COL_MODISAKTIF, 1)
    ->order_by(TBL_MMODUL.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_MMODUL, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/subscription/modul-view/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-view" data-toggle="tooltip" data-placement="top" data-title="Detil"><i class="fas fa-info-circle"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.base_url().'assets/media/modules/'.$r[COL_MODFILENAME].'" class="btn btn-xs btn-outline-info" data-toggle="tooltip" data-placement="top" data-title="Download" target="_blank"><i class="fas fa-download"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_KATEGORI].' - '.$r[COL_MODTITLE],
        (strlen($r[COL_MODDESC]) > 100 ? substr($r[COL_MODDESC], 0, 100) . "..." : $r[COL_MODDESC]),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function modul_view($id) {
    $data ['modul'] = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_MMODUL)
    ->row_array();

    $this->load->view('site/sub/view-modul', $data);
  }
}
?>
