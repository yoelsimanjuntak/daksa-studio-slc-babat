<?php
class Sess extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      if (!$this->input->is_ajax_request()) {
        redirect('site/user/login');
      } else {
        ShowJsonError('HARAP LOGIN TERLEBIH DAHULU!');
        exit();
      }
    }
  }

  public function __calculateTest($id) {
    $rtest = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSESSIONTEST)
    ->row_array();
    if(empty($rtest)) {
      return false;
    }

    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $rtest[COL_IDSESSION])
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsess)) {
      return false;
    }

    $sessScore = null;
    $rformula = $this->db
    ->where(COL_IDPACKAGE, $rsess[COL_IDPACKAGE])
    ->where(COL_IDTEST, $rtest[COL_IDTEST])
    ->get(TBL_MTESTFORMULA)
    ->result_array();

    if(!empty($rformula)) {
      $sessScore = 0;
    }

    foreach($rformula as $f) {
      $val = 0;
      $formula = json_decode($f[COL_SCOREFORMULA]);
      if($f[COL_SCOREREF]=='SCORE') {
        $rscore = $this->db
        ->select_sum(COL_QUESTSCORE)
        ->where(COL_IDTEST, $rtest[COL_UNIQ])
        ->get(TBL_TSESSIONSHEET)
        ->row_array();

        if($rscore) {
          $val = $rscore[COL_QUESTSCORE];
        }
      } else if($f[COL_SCOREREF]=='AVG') {
        $rscore = $this->db
        ->select_sum(COL_QUESTSCORE)
        ->where(COL_IDTEST, $rtest[COL_UNIQ])
        ->get(TBL_TSESSIONSHEET)
        ->row_array();

        if($rscore) {
          $val = $rscore[COL_QUESTSCORE] / $rtest[COL_TESTQUESTNUM];
          $val = round($val);
        }
      } else if($f[COL_SCOREREF]=='TRUE') {
        $count = $this->db
        ->where(COL_QUESTSCORE." > ", 0)
        ->where(COL_IDTEST, $rtest[COL_UNIQ])
        ->count_all_results(TBL_TSESSIONSHEET);

        if($count) {
          $val = $count;
        }
      } else if($f[COL_SCOREREF]=='FALSE') {
        $count = $this->db
        ->where(COL_QUESTSCORE." = ", 0)
        ->where(COL_IDTEST, $rtest[COL_UNIQ])
        ->count_all_results(TBL_TSESSIONSHEET);

        if($count) {
          $val = $count;
        }
      }

      foreach($formula as $for) {
        if($val>=$for->ValueFrom && $val<=$for->ValueTo) {
          $sessScore += toNum($for->Point);
        }
      }
    }

    return $sessScore;
  }

  public function __calculateEPPS($id) {
    $rtest = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSESSIONTEST)
    ->row_array();
    if(empty($rtest)) {
      return false;
    }

    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $rtest[COL_IDSESSION])
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsess)) {
      return false;
    }

    $rsheet = $this->db
    ->where(COL_IDTEST, $id)
    ->get(TBL_TSESSIONSHEET)
    ->result_array();
    if(empty($rsheet)) {
      return false;
    }

    $eppsKode = $rsess[COL_SESSREMARK2];
    $eppsFaktor = $this->db->group_by(COL_MAT_FAKTOR)->order_by(COL_UNIQ, 'asc')->get(TBL_EPPS_MATRIXCATEGORY)->result_array();

    $eppsSess = array(
      COL_IDSESSION=>$rtest[COL_IDSESSION],
      COL_IDTEST=>$id,
      COL_EPPSKODE=>$eppsKode
    );
    $res = $this->db->insert(TBL_EPPS_SESSION, $eppsSess);
    if(!$res) {
      return false;
    }
    $eppsSessId = $this->db->insert_id();
    $eppsRes = array();

    foreach($eppsFaktor as $f) {
      $sumScore = 0;
      $rformula = $this->db
      ->where(COL_EFORM_FAKTOR, $f[COL_MAT_FAKTOR])
      ->order_by(COL_EFORM_NO)
      ->get(TBL_EPPS_FORMULA)
      ->result_array();

      /* get score */
      foreach($rformula as $form) {
        $no = $form[COL_EFORM_NO]-1;

        if($form[COL_EFORM_TIPE]=='KEY') {
          if($rsheet[$no][COL_QUESTRESPONSE] == $form[COL_EFORM_VAL]) {
            $sumScore += 1;
          }
        } else if($form[COL_EFORM_TIPE]=='EQUAL') {
          $no_ = $form[COL_EFORM_VAL]-1;
          if($rsheet[$no][COL_QUESTRESPONSE] == $rsheet[$no_][COL_QUESTRESPONSE]) {
            $sumScore += 1;
          }
        }
      }
      /* get score */

      /* get percentile */
      $rpercentile = $this->db
      ->where(COL_MAT_KODE, $eppsKode)
      ->where(COL_MAT_FAKTOR, $f[COL_MAT_FAKTOR])
      ->where(COL_MAT_SCORE, $sumScore)
      ->get(TBL_EPPS_MATRIX)
      ->row_array();
      /* get percentile */

      /* get category */
      $rcategory = array();
      if(!empty($rpercentile)) {
        $rcategory = $this->db
        ->where(COL_MAT_FAKTOR, $f[COL_MAT_FAKTOR])
        ->where(COL_MAT_PERCENTILE.' <= ', $rpercentile[COL_MAT_PERCENTILE])
        ->order_by(COL_MAT_PERCENTILE, 'desc')
        ->get(TBL_EPPS_MATRIXCATEGORY)
        ->row_array();
      }
      /* get category */

      $eppsRes[] = array(
        COL_EPPSSESSID=>$eppsSessId,
        COL_EPPSFAKTOR=>$f[COL_MAT_FAKTOR],
        COL_EPPSSCORE=>$sumScore,
        COL_EPPSPERCENTILE=>!empty($rpercentile)?$rpercentile[COL_MAT_PERCENTILE]:null,
        COL_EPPSCATEGORY=>!empty($rcategory)?$rcategory[COL_MAT_CATEGORY]:null
      );
    }

    $res = $this->db->insert_batch(TBL_EPPS_SESSIONDET, $eppsRes);
    if(!$res) {
      return false;
    }

    return true;
  }

  public function __calculateIST($id) {
    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, users.DateBirth, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $id)
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsess)) {
      return false;
    }

    $arrIST = array();
    $rkomponen = $this->db
    ->order_by(COL_SEQ)
    ->get(TBL_IST_KOMPONEN)
    ->result_array();

    $sumRW = 0;
    $seq = 0;
    foreach($rkomponen as $r) {
      $age_ = 0;
      if(!empty($rsess[COL_DATEBIRTH])) {
        $age_ = date('Y') - date('Y', strtotime($rsess[COL_DATEBIRTH]));
      }

      $rtest = $this->db
      ->select("*, (select sum(s.QuestScore) as QuestScore from tsessionsheet s where s.IDTest = tsessiontest.Uniq) as QuestScore")
      ->where(COL_IDSESSION, $id)
      ->where(COL_TESTTYPE, 'IST')
      ->where(COL_TESTREMARKS1, $r[COL_KODE])
      ->get(TBL_TSESSIONTEST)
      ->row_array();

      /*if(empty($rtest)) {
        return false;
      }*/

      $rmatrix = $this->db
      ->where(COL_KODE, $r[COL_KODE])
      ->where(COL_RW, (!empty($rtest)?$rtest['QuestScore']:0))
      ->where("(Age=$age_ or (Age>=$age_ and Operand = '<=') or (Age<=$age_ and Operand = '>='))")
      ->order_by(COL_AGE)
      ->get(TBL_IST_KOMPONEN_MATRIX)
      ->row_array();

      $nmKategori = 'Sangat Rendah';
      $nmIQ = '-';
      if(!empty($rmatrix)) {
        if($rmatrix[COL_SW]>118) $nmKategori = 'Sangat Tinggi';
        else if($rmatrix[COL_SW]>104) $nmKategori = 'Tinggi';
        else if($rmatrix[COL_SW]>94) $nmKategori = 'Sedang';
        else if($rmatrix[COL_SW]>80) $nmKategori = 'Rendah';
      }
      $sumRW += (!empty($rtest)?$rtest['QuestScore']:0);

      $arrIST[] = array(
        COL_IDSESSION=>$id,
        COL_KODE=>$r[COL_KODE],
        COL_SEQ=>$r[COL_SEQ],
        COL_RW=>(!empty($rtest)?$rtest['QuestScore']:0),
        COL_SW=>!empty($rmatrix)?$rmatrix[COL_SW]:0,
        COL_KATEGORI=>$nmKategori
      );

      $seq = $r[COL_SEQ];
    }

    $rmapIQ = array();
    $rmapSW = $this->db
    ->where("RW <= $sumRW")
    ->where("(Age=$age_ or (Age>=$age_ and Operand = '<=') or (Age<=$age_ and Operand = '>='))")
    ->order_by(COL_AGE)
    ->order_by(COL_RW, 'desc')
    ->get(TBL_IST_IQ_MAP)
    ->row_array();

    if(!empty($rmapSW)) {
      $rmapIQ = $this->db
      ->where(COL_RW." <= ", $rmapSW[COL_SW])
      ->where("(Age=$age_ or (Age>=$age_ and Operand = '<=') or (Age<=$age_ and Operand = '>='))")
      ->order_by(COL_AGE)
      ->order_by(COL_RW, 'desc')
      ->get(TBL_IST_IQ_MAP)
      ->row_array();

      if(!empty($rmapIQ)) {
        if($rmapIQ[COL_IQ]>139) $nmIQ = 'Genius';
        else if($rmapIQ[COL_IQ]>127) $nmIQ = 'Very Superior';
        else if($rmapIQ[COL_IQ]>119) $nmIQ = 'Superior';
        else if($rmapIQ[COL_IQ]>110) $nmIQ = 'High Average';
        else if($rmapIQ[COL_IQ]>90) $nmIQ = 'Average';
        else if($rmapIQ[COL_IQ]>79) $nmIQ = 'Low Average';
        else if($rmapIQ[COL_IQ]>65) $nmIQ = 'Borderline Defective';
      }
    }

    $arrIST[] = array(
      COL_IDSESSION=>$id,
      COL_KODE=>'JML',
      COL_SEQ=>$seq+1,
      COL_RW=>$sumRW,
      COL_SW=>!empty($rmapSW)?$rmapSW[COL_SW]:0,
      COL_KATEGORI=>null
    );

    $arrIST[] = array(
      COL_IDSESSION=>$id,
      COL_KODE=>'IQ',
      COL_SEQ=>$seq+2,
      COL_RW=>!empty($rmapIQ)?$rmapIQ[COL_IQ]:0,
      COL_SW=>null,
      COL_KATEGORI=>$nmIQ
    );

    $res = $this->db->insert_batch(TBL_IST_RESULT, $arrIST);
    if(!$res) {
      return false;
    }

    return true;
  }

  public function index($stat) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      show_error('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }
    if ($stat=='active') $data['title'] = "Sesi Test - Berlangsung";
    else if ($stat=='complete') $data['title'] = "Sesi Test - Selesai";
    else $data['title'] = "Sesi Test";

    $data['stat'] = $stat;
    $this->template->load('adminlte', 'sess/index', $data);
  }

  public function index_load($stat) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $sessPkg = !empty($_POST['filterPackage'])?$_POST['filterPackage']:null;
    $sessCond = ' 1=1 ';
    if($stat == 'new') {
      $sessCond .= ' and SessTimeStart is null ';
    } else if($stat == 'active') {
      $sessCond .= ' and SessTimeStart is not null and SessTimeEnd is null ';
    } else if($stat == 'complete') {
      $sessCond .= ' and SessTimeEnd is not null ';
    }

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_PKGNAME,COL_FULLNAME,COL_EMAIL,null,COL_CREATEDON);
    if($stat=='active') {
      $orderables = array(null,COL_PKGNAME,COL_FULLNAME,COL_SESSTIMESTART,COL_TESTNAME,null);
    }
    if($stat=='complete') {
      $orderables = array(null,COL_PKGNAME,COL_FULLNAME,COL_SESSTIMESTART,COL_SESSTIMEEND,null);
    }
    $cols = array(COL_PKGNAME, COL_FULLNAME, COL_EMAIL);

    $queryAll = $this->db
    ->where($sessCond)
    ->get(TBL_TSESSION);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($sessPkg)) {
      $this->db->where(COL_IDPACKAGE, $sessPkg);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName, (select sum(TestScore) from tsessiontest ts where ts.IDSession=tsession.Uniq) as SessScore')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where($sessCond)
    ->get_compiled_select(TBL_TSESSION, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/sess/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i>&nbsp;BATALKAN</a>&nbsp;';

      if($stat=='new') {
        $data[] = array(
          $htmlBtn,
          $r[COL_PKGNAME],
          $r[COL_FULLNAME],
          $r[COL_EMAIL],
          $r[COL_PHONE],
          date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
        );
      } else if ($stat=='active') {
        $rtestCurrent = $this->db
        ->query("select * from tsessiontest where TestStart is not null and TestEnd is null and IDSession=".$r[COL_UNIQ]." order by TestSeq")
        ->row_array();

        $timeRemain = '-';
        if(!empty($rtestCurrent)) {
          $timeDue = date_create($rtestCurrent[COL_TESTLIMIT]);
          $timeNow = date_create(date('Y-m-d H:i:s'));
          if($timeDue<=$timeNow) {
            $timeRemain = '<strong class="text-danger float-left">WAKTU HABIS</strong>';
          } else {
            $timeInt = date_diff($timeDue, $timeNow);
            $timeRemain = str_pad($timeInt->format("%h"),2,"0").":".str_pad($timeInt->format("%i"),2,"0").":".str_pad($timeInt->format("%s"),2,"0");
          }
        }

        $data[] = array(
          '<a href="'.site_url('site/sess/reset/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-action" data-toggle="tooltip" data-placement="top" data-title="Reset"><i class="fas fa-sync"></i></a>',
          $r[COL_PKGNAME],
          $r[COL_FULLNAME],
          date('Y-m-d H:i', strtotime($r[COL_SESSTIMESTART])),
          !empty($rtestCurrent)?$rtestCurrent[COL_TESTNAME]:'-',
          $timeRemain
        );
      } else if ($stat=='complete') {
        $rtest = $this->db
        ->query("select * from tsessiontest where TestRemarks1 is null and IDSession=".$r[COL_UNIQ])
        ->result_array();

        //echo $this->db->last_query();
        //exit();

        $sum=0;
        foreach($rtest as $t) {
          $rquest = $this->db
          ->query("select sum(QuestScore) as QuestScore from tsessionsheet where IDTest=".$t[COL_UNIQ])
          ->row_array();

          /*if($t[COL_TESTNAME]!='Pass Hand A4') {
            $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
          }*/
          if(!(strpos(strtolower($t[COL_TESTNAME]), 'pass hand') !== false)) {
            $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
          }
        }

        $data[] = array(
          '<a href="'.site_url('site/sess/result/'.$r[COL_UNIQ]).'" class="btn btn-primary btn-xs"><i class="far fa-info-circle"></i>&nbsp;HASIL</a>',
          $r[COL_PKGNAME],
          $r[COL_FULLNAME],
          date('Y-m-d H:i', strtotime($r[COL_SESSTIMESTART])),
          date('Y-m-d H:i', strtotime($r[COL_SESSTIMEEND])),
          '<strong>'.number_format($sum).'</strong>'
        );
      }
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      show_error('ANDA TIDAK MEMILIKI AKSES!');
      exit();
    }

    if(!empty($_POST) && $this->input->is_ajax_request()) {
      $rparticipant = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_USERNAME))
      ->get(TBL_USERS)
      ->row_array();

      if(empty($rparticipant)) {
        ShowJsonError('PARAMETER TIDAK VALID!');
        exit();
      }
      if($rparticipant[COL_ISSUSPEND]==1) {
        ShowJsonError('Mohon maaf, akun pengguna an. <strong>'.$rparticipant[COL_FULLNAME].'</strong> sedang berstatus SUSPEND. Silakan ubah status akun terlebih dahulu.');
        exit();
      }

      $dat = array(
        COL_IDPACKAGE=>$this->input->post(COL_IDPACKAGE),
        COL_USERNAME=>$this->input->post(COL_USERNAME),
        COL_SESSREMARK2=>$this->input->post(COL_SESSREMARK2),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      if(empty($this->input->post('IsRandom')) || $this->input->post('IsRandom') != 1) {
        $dat[COL_SESSREMARK1] = 'NORANDOM';
      }

      $res = $this->db->insert(TBL_TSESSION, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Penambahan Sesi Test an. <strong>'.$rparticipant[COL_FULLNAME].'</strong> berhasil.');
      exit();
    } else {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }
  }

  public function delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TSESSION);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function start($id) {
    $ruser = GetLoggedUser();
    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $id)
    ->get(TBL_TSESSION)
    ->row_array();

    if(empty($rsess)) {
      if ($this->input->is_ajax_request()) ShowJsonError('PARAMETER TIDAK VALID!');
      else show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    if(!empty($rsess[COL_SESSTIMEEND])) {
      if ($this->input->is_ajax_request()) ShowJsonError('SESI INI TELAH BERAKHIR!');
      else redirect('site/sess/result/'.$id);
      exit();
    }

    if($ruser[COL_USERNAME] != $rsess[COL_USERNAME]) {
      if ($this->input->is_ajax_request()) ShowJsonError('AKSES ANDA TIDAK VALID!');
      else show_error('AKSES ANDA TIDAK VALID!');
      exit();
    }

    if(empty($ruser[COL_DATEBIRTH])) {
      if ($this->input->is_ajax_request()) ShowJsonError('HARAP MELENGKAPI DATA TANGGAL LAHIR TERLEBIH DAHULU!');
      else show_error('HARAP MELENGKAPI DATA TANGGAL LAHIR TERLEBIH DAHULU!');
      exit();
    }

    /* jika baru pertama kali start, maka insert data test yang diperlukan */
    if(empty($rsess[COL_SESSTIMESTART]) && $this->input->is_ajax_request()) {
      $this->db->trans_begin();
      try {
        $res = $this->db
        ->where(COL_UNIQ, $id)
        ->update(TBL_TSESSION, array(COL_SESSTIMESTART=>date('Y-m-d H:i:s')));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $rpackage = $this->db
        ->where('PkgItems is not null')
        ->where(COL_PKGISACTIVE, 1)
        ->where(COL_UNIQ, $rsess[COL_IDPACKAGE])
        ->get(TBL_MTESTPACKAGE)
        ->row_array();
        if(empty($rpackage)) {
          throw new Exception('Maaf, paket tidak tersedia untuk saat ini. Silakan hubungi administrator.');
        }

        $arrPkgItems = json_decode($rpackage[COL_PKGITEMS]);
        if(empty($arrPkgItems)) {
          throw new Exception('Maaf, paket tidak tersedia untuk saat ini. Silakan hubungi administrator. <br /><br /><strong>[code: PKGITEM]</strong>');
        }

        /* create test */
        $arrTest = array();
        $seq = 1;
        foreach($arrPkgItems as $i) {
          $rtest = $this->db
          ->where(COL_UNIQ, $i->TestID)
          ->get(TBL_MTEST)
          ->row_array();
          if(empty($rtest)) {
            throw new Exception('Maaf, paket tidak tersedia untuk saat ini. Silakan hubungi administrator. <br /><br /><strong>[code: PKGTEST]</strong>');
          }

          $arrTest[] = array(
            COL_IDSESSION => $rsess[COL_UNIQ],
            COL_IDTEST => $rtest[COL_UNIQ],
            COL_TESTSEQ => $seq,
            COL_TESTTYPE => $rtest[COL_TESTTYPE],
            COL_TESTNAME => $rtest[COL_TESTNAME],
            COL_TESTQUESTNUM => $rtest[COL_TESTQUESTNUM],
            COL_TESTQUESTSUB => $rtest[COL_TESTQUESTSUB],
            COL_TESTDURATION => $rtest[COL_TESTDURATION],
            COL_TESTINSTRUCTION => $rtest[COL_TESTINSTRUCTION],
            COL_TESTSYMBOL => $rtest[COL_TESTSYMBOL],
            COL_TESTREMARKS1 => $rtest[COL_TESTREMARKS1]
          );
          $seq++;
        }
        $res = $this->db->insert_batch(TBL_TSESSIONTEST, $arrTest);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
        /* create test */

        /* create soal */
        $arrQuest = array();
        $rsessiontest = $this->db
        ->where(COL_IDSESSION, $rsess[COL_UNIQ])
        ->order_by(COL_TESTSEQ, 'asc')
        ->get(TBL_TSESSIONTEST)
        ->result_array();

        foreach($rsessiontest as $t) {
          $num = $t[COL_TESTQUESTNUM];
          if($t[COL_TESTTYPE]!='ACR'&&$t[COL_TESTTYPE]!='PAULI' /*$t[COL_TESTTYPE]=='MUL'*/) {
            $numRemain = $num;

            while($numRemain>0) {
              $rquest = $this->db
              ->where(COL_IDTEST, $t[COL_IDTEST])
              ->where(COL_QUESTISACTIVE, 1)
              ->order_by(($rsess[COL_SESSREMARK1]!='NORANDOM'&&$t[COL_TESTREMARKS1]!='EPPS'&&$t[COL_TESTREMARKS1]!='IST'?'rand()':COL_UNIQ))
              ->get(TBL_MTESTDETAIL, $numRemain)
              ->result_array();

              /*if(count($rquest)<$num) {
                throw new Exception('Maaf, paket tidak tersedia untuk saat ini. Silakan hubungi administrator.<br /><br /><strong>[code: QUESTNUMBER]</strong>');
              }*/
              if(count($rquest)==$num) {
                $numRemain=0;
              } else {
                $numRemain=$numRemain-count($rquest);
              }

              foreach($rquest as $q) {
                $arrQuest[] = array(
                  COL_IDTEST=>$t[COL_UNIQ],
                  COL_QUESTTYPE=>$q[COL_QUESTTYPE],
                  COL_QUESTGROUP=>$q[COL_QUESTGROUP],
                  COL_QUESTTEXT=>$q[COL_QUESTTEXT],
                  COL_QUESTTEXTSUB=>null,
                  COL_QUESTIMAGE=>$q[COL_QUESTIMAGE],
                  COL_QUESTOPTION=>$q[COL_QUESTOPTION]
                );

                $arrQuest_ = array(
                  COL_IDTEST=>$t[COL_UNIQ],
                  COL_QUESTTYPE=>$q[COL_QUESTTYPE],
                  COL_QUESTGROUP=>$q[COL_QUESTGROUP],
                  COL_QUESTTEXT=>$q[COL_QUESTTEXT],
                  COL_QUESTTEXTSUB=>null,
                  COL_QUESTIMAGE=>$q[COL_QUESTIMAGE],
                  COL_QUESTOPTION=>$q[COL_QUESTOPTION]
                );
                $res = $this->db->insert(TBL_TSESSIONSHEET, $arrQuest_);
                if(!$res) {
                  $err = $this->db->error();
                  throw new Exception($err['message']);
                }
              }
            }
          } else if($t[COL_TESTTYPE]=='ACR') {
            $numsub = $t[COL_TESTQUESTSUB];
            $arrNum = range(0,9);
            $arrAlpha = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
            $arrSym = explode(',',strip_tags($t[COL_TESTSYMBOL]));
            $arrMixed = array_merge($arrNum, $arrAlpha);

            $arrPtn = array();
            $arrSrc = $arrNum;
            if ($rsess[COL_SESSREMARK2]=='HURUF') $arrSrc = $arrAlpha;
            else if ($rsess[COL_SESSREMARK2]=='SIMBOL') {
              if(count($arrSym)<5) {
                throw new Exception('Maaf, paket tidak tersedia untuk saat ini. Silakan hubungi administrator. <br /><br /><strong>[code: PKGSYM]</strong>');
              }
              $arrSrc = $arrSym;
            }
            else if ($rsess[COL_SESSREMARK2]=='MIXED') $arrSrc = $arrMixed;

            for($i=0; $i<$num; $i++) {
              $ptn = '';
              $ptnIsExist = 1;
              while($ptnIsExist==1) {
                $ptnShuffle = $arrSrc;
                shuffle($ptnShuffle);
                $ptn = implode(",", array_slice($ptnShuffle, 0, 5));
                if(!in_array($ptn, $arrPtn)) {
                  $ptnIsExist = 0;
                  $arrPtn[] = $ptn;
                }
              }
            }

            foreach($arrPtn as $ptn) {
              $arrSoal = array();
              $ptnArr = explode(",", $ptn);
              for($sub=0; $sub<$numsub; $sub++) {
                $soal = '';
                $soalIsExist = 1;

                while($soalIsExist==1) {
                  $soalShuffle = $ptnArr;
                  shuffle($soalShuffle);
                  $soalArr = array_slice($soalShuffle, 0, 4);
                  $soal = implode(",", $soalArr);
                  if(!in_array($soal, $arrSoal)) {
                    $soalIsExist = 0;
                    $soalOpt = array(
                      array('Opt'=>'A', 'Txt'=>$ptnArr[0], 'Val'=>(in_array($ptnArr[0], $soalArr)?0:1)),
                      array('Opt'=>'B', 'Txt'=>$ptnArr[1], 'Val'=>(in_array($ptnArr[1], $soalArr)?0:1)),
                      array('Opt'=>'C', 'Txt'=>$ptnArr[2], 'Val'=>(in_array($ptnArr[2], $soalArr)?0:1)),
                      array('Opt'=>'D', 'Txt'=>$ptnArr[3], 'Val'=>(in_array($ptnArr[3], $soalArr)?0:1)),
                      array('Opt'=>'E', 'Txt'=>$ptnArr[4], 'Val'=>(in_array($ptnArr[4], $soalArr)?0:1))
                    );

                    $arrSoal[] = $soal;
                    $arrQuest[] = array(
                      COL_IDTEST=>$t[COL_UNIQ],
                      COL_QUESTTEXT=>$ptn,
                      COL_QUESTTEXTSUB=>$soal,
                      COL_QUESTIMAGE=>null,
                      COL_QUESTOPTION=>json_encode($soalOpt)
                    );

                    $arrQuest_ = array(
                      COL_IDTEST=>$t[COL_UNIQ],
                      COL_QUESTTEXT=>$ptn,
                      COL_QUESTTEXTSUB=>$soal,
                      COL_QUESTIMAGE=>null,
                      COL_QUESTOPTION=>json_encode($soalOpt)
                    );
                    $res = $this->db->insert(TBL_TSESSIONSHEET, $arrQuest_);
                    if(!$res) {
                      $err = $this->db->error();
                      throw new Exception($err['message']);
                    }
                  }
                }
              }
            }
          } else if($t[COL_TESTTYPE]=='PAULI') {
            $numsub = $t[COL_TESTQUESTSUB];
            $arrNum = range(0,9);

            for($i=0; $i<$num*$numsub; $i++) {
              $ptn = '';
              $ptnShuffle = $arrNum;
              //shuffle($ptnShuffle);

              $ptn = rand(0,9).",".rand(0,9); //implode(",", array_slice($ptnShuffle, 0, 2));
              $arrPtn[] = $ptn;
            }

            $idx = 0;
            for($i=0; $i<$num; $i++) {
              $qtext = 'KOLOM - '.($i+1);
              for($sub=0; $sub<$numsub; $sub++) {
                $_opr = str_replace(",",$t[COL_TESTSYMBOL],$arrPtn[$idx]);
                $_key = eval('return '.$_opr.';');
                $_key = substr((string) $_key, -1);
                $_opt = array(
                  array('Opt'=>$_key, 'Txt'=>$_key, 'Val'=>1)
                );
                $arrQuest_ = array(
                  COL_QUESTTYPE=>'TEXT',
                  COL_IDTEST=>$t[COL_UNIQ],
                  COL_QUESTTEXT=>$qtext,
                  COL_QUESTTEXTSUB=>$arrPtn[$idx],
                  COL_QUESTIMAGE=>null,
                  COL_QUESTOPTION=>json_encode($_opt)
                );
                $arrQuest[] = $arrQuest_;
                $res = $this->db->insert(TBL_TSESSIONSHEET, $arrQuest_);
                if(!$res) {
                  $err = $this->db->error();
                  throw new Exception($err['message']);
                }
                $idx++;
              }
            }
          }
        }

        if(empty($arrQuest)) {
          throw new Exception('Maaf, sedang terjadi kesalahan pada sistem saat ini. Silakan hubungi administrator.<br /><br /><strong>[code: QUESTEMPTY]</strong>');
        }

        /*$res = $this->db->insert_batch(TBL_TSESSIONSHEET, $arrQuest);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }*/
        /* create soal */

        $this->db->trans_commit();
        ShowJsonSuccess('READY', array('redirect'=>current_url()));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else if(empty($rsess[COL_SESSTIMESTART]) && !$this->input->is_ajax_request()) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    } else {
      $idSess = $rsess[COL_UNIQ];
      /* check overdue test */
      $rtestOutstanding = $this->db
      ->where("IDSession=$idSess and TestLimit is not null and TestLimit <= '".date('Y-m-d H:i:s')."'")
      ->get(TBL_TSESSIONTEST)
      ->result_array();

      foreach($rtestOutstanding as $t) {
        $testScore = $this->__calculateTest($t[COL_UNIQ]);
        if ($testScore===false) {

        } else {
          $res = $this->db
          ->where(COL_UNIQ, $t[COL_UNIQ])
          ->update(TBL_TSESSIONTEST, array(COL_TESTSCORE=>$testScore));
        }
      }
      /* check overdue test */

      /* end overdue test */
      $res = $this->db->query("update tsessiontest set TestRemarks='WAKTU HABIS', TestEnd=TestLimit where IDSession=$idSess and TestLimit is not null and TestLimit <= '".date('Y-m-d H:i:s')."'");
      /* end overdue test */

      /* re-check overdue test */
      $rtestPending = $this->db
      ->where("IDSession=$idSess and TestEnd is null")
      ->get(TBL_TSESSIONTEST)
      ->result_array();
      if(empty($rtestPending)) {
        if(empty($rsess[COL_SESSTIMEEND])) {
          $rtestLast = $this->db
          ->where("IDSession=$idSess")
          ->order_by(COL_TESTEND, 'desc')
          ->get(TBL_TSESSIONTEST)
          ->row_array();
          if(!empty($rtestLast)) {
            $res = $this->db->query("update tsession set SessTimeEnd='".date('Y-m-d H:i:s', strtotime($rtestLast[COL_TESTEND]))."' where Uniq=$idSess");
          }
        }

        redirect(site_url('site/sess/result/'.$rsess[COL_UNIQ]));
      }
      /* re-check overdue test */

      $this->load->view('site/sess/sheet', array('sess'=>$rsess, 'title'=>$rsess[COL_PKGNAME]));
    }
  }

  public function start_test($id) {
    $ruser = GetLoggedUser();
    $rtest = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSESSIONTEST)
    ->row_array();
    if(empty($rtest)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $rtest[COL_IDSESSION])
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsess)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    if(!empty($rsess[COL_SESSTIMEEND])) {
      if ($this->input->is_ajax_request()) ShowJsonError('SESI INI TELAH BERAKHIR!');
      else redirect('site/sess/result/'.$id);
      exit();
    }

    if($ruser[COL_USERNAME] != $rsess[COL_USERNAME]) {
      if ($this->input->is_ajax_request()) ShowJsonError('AKSES ANDA TIDAK VALID!');
      else show_error('AKSES ANDA TIDAK VALID!');
      exit();
    }

    if(empty($rtest[COL_TESTSTART])) {
      $dur = $rtest[COL_TESTDURATION];
      if(!empty($rtest[COL_TESTQUESTSUB])) {
        $dur = $rtest[COL_TESTDURATION]*$rtest[COL_TESTQUESTNUM];
        /* toleransi */
        $dur += 10;
        /* toleransi */
      }
      $secs = $dur*60;
      $res = $this->db
      ->where(COL_UNIQ, $id)
      ->update(TBL_TSESSIONTEST, array(COL_TESTSTART=>date('Y-m-d H:i:s'), COL_TESTLIMIT=>date('Y-m-d H:i:s', strtotime("+{$secs} seconds"))));
      if(!$res) {
        show_error('Mohon maaf, telah terjadi kesalahan pada sistem. Silakan <a href="'.current_url().'"><strong>RELOAD</strong></a> atau kembali ke <a href="'.site_url('site/user/dashboard').'"><strong>HALAMAN UTAMA</strong></a>.');
        exit();
      }
    }
    if(!empty($rtest[COL_TESTEND])) {
      //redirect('site/sess/start/'.$rsess[COL_UNIQ]);
    }
    if(!empty($rtest[COL_TESTLIMIT]) && strtotime($rtest[COL_TESTLIMIT])-strtotime(date('Y-m-d H:i:s'))<=0) {
      $res = $this->db
      ->where(COL_UNIQ, $id)
      ->update(TBL_TSESSIONTEST, array(COL_TESTEND=>$rtest[COL_TESTLIMIT], COL_TESTREMARKS=>'WAKTU HABIS'));
      if(!$res) {
        show_error('Mohon maaf, telah terjadi kesalahan pada sistem. Silakan <a href="'.current_url().'"><strong>RELOAD</strong></a> atau kembali ke <a href="'.site_url('site/user/dashboard').'"><strong>HALAMAN UTAMA</strong></a>.');
        exit();
      }
    }

    redirect('site/sess/start/'.$rsess[COL_UNIQ]);
    //$this->load->view('site/sess/sheet', array('sess'=>$rsess, 'title'=>$rsess[COL_PKGNAME]));
  }

  public function reset($id) {
    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $id)
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsess)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $this->db->trans_begin();
    try {
      $res = $this->db
      ->where(COL_UNIQ, $id)
      ->update(TBL_TSESSION, array(COL_SESSTIMESTART=>null, COL_SESSTIMEEND=>null));
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }

      $res = $this->db->where(COL_IDSESSION, $id)->delete(TBL_TSESSIONTEST);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception($err['message']);
      }
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }
    $this->db->trans_commit();
    ShowJsonSuccess('Reset Berhasil.');
  }

  public function submit($id) {
    $rtest = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSESSIONTEST)
    ->row_array();
    if(empty($rtest)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $rtest[COL_IDSESSION])
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsess)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $rquest = $this->db
    ->where(COL_IDTEST, $rtest[COL_UNIQ])
    ->order_by(COL_UNIQ,'asc')
    ->get(TBL_TSESSIONSHEET)
    ->result_array();

    $isFinish = $this->input->post('finish');

    $this->db->trans_begin();
    try {
      foreach($rquest as $q) {
        $opt = json_decode($q[COL_QUESTOPTION]);
        $resp = $this->input->post("QuestResponse__".$q[COL_UNIQ]);
        $score = 0;

        if(!empty($opt) && is_array($opt)) {
          foreach($opt as $o) {
            if(strtolower($o->Opt) == strtolower($resp)) {
              $score = toNum($o->Val);
              break;
            }
          }
        }

        if(($rtest[COL_TESTTYPE]=='ACR'||$rtest[COL_TESTTYPE]=='PAULI') && (empty($isFinish) || !$isFinish)) {
          // do nothing on ACR test and finish flag empty
        } else {
          if(!empty($resp)) {
            $res = $this->db
            ->where(COL_UNIQ, $q[COL_UNIQ])
            ->update(TBL_TSESSIONSHEET, array(COL_QUESTRESPONSE=>empty($resp)?null:$resp, COL_QUESTSCORE=>$score));
            if(!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }
        }
      }

      if(!empty($isFinish) && $isFinish) {
        //$this->db->trans_commit();
        $endTime = date('Y-m-d H:i:s');
        $endRemarks = null;
        if($endTime > $rtest[COL_TESTLIMIT]) {
          $endRemarks = 'WAKTU HABIS';
        }

        $sessScore = 0;
        if ($rtest[COL_TESTREMARKS1]=='EPPS') {
          $resEPPS = $this->__calculateEPPS($id);
          if ($resEPPS===false) {
            throw new Exception('Maaf, sedang terjadi kesalahan pada sistem saat ini. Silakan hubungi administrator.<br /><br /><strong>[code: CALCEPPS]</strong>');
          } else {
            //throw new Exception('score = '.json_encode($sessScore));
          }
        } else {
          $sessScore = $this->__calculateTest($id);
          if ($sessScore===false) {
            throw new Exception('Maaf, sedang terjadi kesalahan pada sistem saat ini. Silakan hubungi administrator.<br /><br /><strong>[code: CALCSCORE]</strong>');
          } else {
            //throw new Exception('score = '.json_encode($sessScore));
          }
        }

        //$this->db->trans_begin();
        $res = $this->db
        ->where(COL_UNIQ, $id)
        ->update(TBL_TSESSIONTEST, array(COL_TESTEND=>$endTime, COL_TESTREMARKS=>$endRemarks, COL_TESTSCORE=>$sessScore));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $res = $this->db
        ->where(COL_UNIQ, $id)
        ->update(TBL_TSESSIONTEST, array(COL_TESTEND=>$endTime, COL_TESTREMARKS=>$endRemarks, COL_TESTSCORE=>$sessScore));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        /* clean null-response sheet record */
        if($rtest[COL_TESTTYPE]=='ACR'||$rtest[COL_TESTTYPE]=='PAULI') {
          $res = $this->db->where(COL_IDTEST, $id)->where('QuestResponse is null')->delete(TBL_TSESSIONSHEET);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }
        /* clean null-response sheet record */

        /* check wheter all test done */
        $isDone = false;
        $rtestOutstanding = $this->db
        ->where('TestEnd is null')
        ->where(COL_IDSESSION, $rsess[COL_UNIQ])
        ->order_by(COL_TESTSEQ, 'asc')
        ->get(TBL_TSESSIONTEST)
        ->row_array();
        if(empty($rtestOutstanding)) {
          $isDone = true;
          $res = $this->db
          ->where(COL_UNIQ, $rsess[COL_UNIQ])
          ->update(TBL_TSESSION, array(COL_SESSTIMEEND=>date('Y-m-d H:i:s')));

          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }
        }
        /* check wheter all test done */
      }

      if($rtest[COL_TESTREMARKS1]=='EPPS') {

      }

      $this->db->trans_commit();

      ShowJsonSuccess('Submit Berhasil!', array('redirect'=>site_url('site/sess/start/'.$rsess[COL_UNIQ])));
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      exit();
    }
  }

  public function result($id) {
    $ruser = GetLoggedUser();
    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $id)
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsess)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_USERNAME] != $rsess[COL_USERNAME]) {
      if ($this->input->is_ajax_request()) ShowJsonError('AKSES ANDA TIDAK VALID!');
      else show_error('AKSES ANDA TIDAK VALID!');
      exit();
    }

    /* check wheter this session is IST */
    $rist = $this->db
    ->where(COL_IDSESSION, $id)
    ->where(COL_TESTTYPE, 'IST')
    ->get(TBL_TSESSIONTEST)
    ->row_array();
    if(!empty($rist)) {
      $rresult = $this->db
      ->where(COL_IDSESSION, $id)
      ->get(TBL_IST_RESULT)
      ->row_array();

      if(empty($rresult)) {
        $this->__calculateIST($id);
      }
    }
    /* check wheter this session is IST */

    $data['title'] = 'Hasil '.$rsess[COL_PKGNAME];
    $data['sess'] = $rsess;
    $this->template->load('adminlte', 'sess/result', $data);
  }

  public function review($id) {
    $ruser = GetLoggedUser();
    $rtest = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSESSIONTEST)
    ->row_array();

    if(empty($rtest)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $rtest[COL_IDSESSION])
    ->get(TBL_TSESSION)
    ->row_array();

    if(empty($rsess)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_USERNAME] != $rsess[COL_USERNAME]) {
      if ($this->input->is_ajax_request()) ShowJsonError('AKSES ANDA TIDAK VALID!');
      else show_error('AKSES ANDA TIDAK VALID!');
      exit();
    }

    $data['title'] = 'Pembahasan '.$rsess[COL_PKGNAME];
    $data['rsess'] = $rsess;
    $data['rtest'] = $rtest;
    $this->template->load('adminlte-top', 'sess/review', $data);
  }

  public function review_print($id) {
    $ruser = GetLoggedUser();
    $rtest = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSESSIONTEST)
    ->row_array();

    if(empty($rtest)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    $rsess = $this->db
    ->select('tsession.*, users.Fullname, users.Email, users.Phone, mtestpackage.PkgName')
    ->join(TBL_MTESTPACKAGE,TBL_MTESTPACKAGE.'.'.COL_UNIQ." = ".TBL_TSESSION.".".COL_IDPACKAGE,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_USERNAME,"left")
    ->where(TBL_TSESSION.'.'.COL_UNIQ, $rtest[COL_IDSESSION])
    ->get(TBL_TSESSION)
    ->row_array();

    if(empty($rsess)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_USERNAME] != $rsess[COL_USERNAME]) {
      if ($this->input->is_ajax_request()) ShowJsonError('AKSES ANDA TIDAK VALID!');
      else show_error('AKSES ANDA TIDAK VALID!');
      exit();
    }

    $data['title'] = 'Pembahasan '.$rsess[COL_PKGNAME];
    $data['rsess'] = $rsess;
    $data['rtest'] = $rtest;
    //$this->template->load('adminlte-top', 'sess/review', $data);

    $this->load->library('Mypdf');
    $mpdf = new Mypdf('','A4',0,'',15,15,36,16);

    $html = $this->load->view('site/sess/review-print', $data, TRUE);
    //echo $html;
    //exit();
    $htmlTitle = strtoupper($this->setting_web_name);
    $htmlTitleSub = 'www.daksastudio.id<br />'.$this->setting_org_mail.' / '.$this->setting_org_phone.' / @id.daksastudio<br />'.$this->setting_org_address;
    $htmlLogo = base_url().$this->setting_web_logo;
    $htmlHeader = @"
    <table style=\"border: none !important\">
      <tr>
        <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold; vertical-align: top; text-align: center\"><b>$htmlTitle</b><br /><small style=\"font-weight: normal\">$htmlTitleSub</small></td>
      </tr>
    </table>
    <hr />
    ";

    $mpdf->pdf->SetTitle($htmlTitle);
    $mpdf->pdf->SetHTMLHeader($htmlHeader);
    //$mpdf->pdf->SetWatermarkImage(MY_IMAGEURL.'logo-full-square.png');
    //$mpdf->pdf->showWatermarkImage = true;
    $mpdf->pdf->SetFooter('Dicetak melalui aplikasi '.$this->setting_web_name);
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->use_kwt = true;
    $mpdf->pdf->Output($this->setting_web_name.' - '.$htmlTitle.'.pdf', 'I');
  }

  public function recalculate($id) {
    $sessScore = $this->__calculateTest($id);
    if ($sessScore===false) {
      //throw new Exception('Maaf, sedang terjadi kesalahan pada sistem saat ini. Silakan hubungi administrator.<br /><br /><strong>[code: CALCSCORE]</strong>');
    } else {
      //throw new Exception('score = '.json_encode($sessScore));
      $res = $this->db
      ->where(COL_UNIQ, $id)
      ->update(TBL_TSESSIONTEST, array(COL_TESTSCORE=>$sessScore));
      echo 'Done';
      exit();
    }
  }

  public function pre_test($id) {
    $rtest = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSESSIONTEST)
    ->row_array();

    if(empty($rtest)) {
      show_error('PARAMETER TIDAK VALID!');
      exit();
    }

    echo $rtest[COL_TESTSYMBOL];
  }
}
?>
