<?php
class Sas extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      if (!$this->input->is_ajax_request()) {
        redirect('site/user/login');
      } else {
        ShowJsonError('HARAP LOGIN TERLEBIH DAHULU!');
        exit();
      }
    }
  }

  public function master_mapel() {
    $data['title'] = "Master Data Mata Pelajaran";
    $this->template->load('adminlte', 'sas/master-mapel', $data);
  }

  public function master_mapel_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    //$questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_MAPELKATEGORI=>'asc');
    $orderables = array(null,COL_MAPELKATEGORI,COL_MAPELNAMA);
    $cols = array(COL_MAPELKATEGORI,COL_MAPELNAMA);

    $queryAll = $this->db
    ->get(TBL_SAS_MMAPEL);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->get_compiled_select(TBL_SAS_MMAPEL, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/sas/master-mapel-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/sas/master-mapel-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui" data-name="'.$r[COL_MAPELNAMA].'" data-kategori="'.$r[COL_MAPELKATEGORI].'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_MAPELKATEGORI],
        $r[COL_MAPELNAMA],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function master_mapel_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_MAPELKATEGORI => $this->input->post(COL_MAPELKATEGORI),
        COL_MAPELNAMA => $this->input->post(COL_MAPELNAMA)
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SAS_MMAPEL, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function master_mapel_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_MAPELKATEGORI => $this->input->post(COL_MAPELKATEGORI),
        COL_MAPELNAMA => $this->input->post(COL_MAPELNAMA)
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_SAS_MMAPEL, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function master_mapel_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_SAS_MMAPEL);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function master_prodi() {
    $data['title'] = "Master Data Prodi";
    $this->template->load('adminlte', 'sas/master-prodi', $data);
  }

  public function master_prodi_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    //$questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_PRODITIPE=>'asc');
    $orderables = array(null,COL_PRODITIPE,COL_PRODIKODE,COL_PRODINAMA,COL_PRODIPT,COL_NUMNILAI);
    $cols = array(COL_PRODITIPE,COL_PRODIKODE,COL_PRODINAMA,COL_PRODIPT);

    $queryAll = $this->db
    ->get(TBL_SAS_MPRODI);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->get_compiled_select(TBL_SAS_MPRODI, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/sas/master-prodi-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/sas/master-prodi-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui" data-name="'.$r[COL_PRODINAMA].'" data-kategori="'.$r[COL_PRODITIPE].'" data-kode="'.$r[COL_PRODIKODE].'" data-pt="'.$r[COL_PRODIPT].'" data-nrm="'.$r[COL_NUMNILAI].'"><i class="fas fa-pencil-alt"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_PRODITIPE],
        $r[COL_PRODIKODE],
        $r[COL_PRODINAMA],
        $r[COL_PRODIPT],
        number_format($r[COL_NUMNILAI],2),
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function master_prodi_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_PRODITIPE => $this->input->post(COL_PRODITIPE),
        COL_PRODINAMA => $this->input->post(COL_PRODINAMA),
        COL_PRODIKODE => $this->input->post(COL_PRODIKODE),
        COL_PRODIPT => $this->input->post(COL_PRODIPT),
        COL_NUMNILAI => toNum($this->input->post(COL_NUMNILAI))
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SAS_MPRODI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function master_prodi_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_PRODITIPE => $this->input->post(COL_PRODITIPE),
        COL_PRODINAMA => $this->input->post(COL_PRODINAMA),
        COL_PRODIKODE => $this->input->post(COL_PRODIKODE),
        COL_PRODIPT => $this->input->post(COL_PRODIPT),
        COL_NUMNILAI => toNum($this->input->post(COL_NUMNILAI))
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_SAS_MPRODI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function master_prodi_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_SAS_MPRODI);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function order() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $rExistSAS = $this->db
      ->where(COL_USERNAME, $ruser[COL_USERNAME])
      ->order_by(COL_CREATEDON, 'desc')
      ->get(TBL_SAS_TFORM)
      ->row_array();

      if(!empty($rExistSAS)) {
        echo 'Mengalihkan ...';
        echo '<script>setTimeout(function(){location.href="'.site_url('site/sas/result/'.$rExistSAS[COL_UNIQ]).'";}, 1000)</script>';
        exit();
      }

      $rExistOrder = $this->db
      ->where(COL_ORDERNAME, $ruser[COL_USERNAME])
      ->get(TBL_SAS_ORDER)
      ->row_array();
      if(!empty($rExistOrder)) {
        if($rExistOrder[COL_ORDERSTATUS]=='NEW') {
          echo '<p>Maaf, pengajuan akses <strong>sedang diproses</strong>. Silakan menunggu beberapa saat lagi atau hubungi Administrator untuk info lebih lanjut.</p>';
          exit();
        } else if($rExistOrder[COL_ORDERSTATUS]=='APPROVED') {
          echo 'Mengalihkan ...';
          echo '<script>javascript:window.open("'.site_url('site/sas/form').'"); setTimeout(function(){location.reload();}, 1000)</script>';
          exit();
        }
      }
    }

    if(!empty($_POST)) {
      $dat =  array(
        COL_ORDERNAME=>$ruser[COL_ROLEID]==ROLEADMIN?$this->input->post(COL_USERNAME):$ruser[COL_USERNAME],
        COL_ORDERSTATUS=>!empty($this->input->post(COL_ORDERSTATUS))?$this->input->post(COL_ORDERSTATUS):'NEW',
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->insert(TBL_SAS_ORDER, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $msg = 'Analisa Peluang SNBP telah diajukan ke Administrator. Silakan menunggu konfirmasi beberapa saat.';
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $msg = 'Penambahan permintaan Analisa Peluang SNBP berhasil.';
      }
      ShowJsonSuccess($msg);
      exit();
    } else {
      $this->load->view('site/sas/order');
    }
  }

  public function order_index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Anda tidak memiliki HAK AKSES!');
      exit();
    }

    $data['title'] = 'Permintaan Analisa SNBP';
    $this->template->load('adminlte', 'sas/order-index', $data);
  }

  public function order_index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_USERNAME,COL_FULLNAME,null,COL_CREATEDON);
    $cols = array(COL_USERNAME,COL_FULLNAME);

    $queryAll = $this->db
    ->get(TBL_SAS_ORDER);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($filterStatus)) {
      $this->db->where(COL_ORDERSTATUS, $filterStatus);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('sas_order.*, users.Fullname, users.Email, users.Phone')
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_SAS_ORDER.".".COL_ORDERNAME,"left")
    ->get_compiled_select(TBL_SAS_ORDER, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/sas/order-changestatus/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-changestatus"><i class="fas fa-sync"></i>&nbsp;UBAH STATUS</a>&nbsp;';
      $htmlStatus = '<span class="badge badge-secondary">NEW</span>';
      if($r[COL_ORDERSTATUS]=='APPROVED') $htmlStatus = '<span class="badge badge-success">APPROVED</span>';
      else if($r[COL_ORDERSTATUS]=='REJECTED') $htmlStatus = '<span class="badge badge-danger">REJECTED</span>';

      $data[] = array(
        $htmlBtn,
        $r[COL_ORDERNAME],
        $r[COL_FULLNAME],
        $htmlStatus,
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function order_changestatus($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('Anda tidak memiliki HAK AKSES!');
      exit();
    }

    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_SAS_ORDER)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    if(!empty($_POST)) {
      $dat =  array(
        COL_ORDERSTATUS=>$this->input->post(COL_ORDERSTATUS),
        COL_UPDATEDBY=>$ruser[COL_USERNAME],
        COL_UPDATEDON=>date('Y-m-d H:i:s')
      );

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_SAS_ORDER, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Perubahan status berhasil.');
      exit();
    } else {
      $data['data'] = $rdata;
      $this->load->view('site/sas/order-status', $data);
    }
  }

  public function index() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      redirect(site_url('site/user/dashboard'));
    }

    $data['title'] = 'Rekapitulasi Hasil Analisa SNBP';
    $this->template->load('adminlte', 'sas/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    //$filterStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_FULLNAME,COL_CREATEDON,COL_FORMASALSEKOLAH);
    $cols = array(COL_USERNAME,COL_FULLNAME,COL_FORMASALSEKOLAH);

    $queryAll = $this->db
    ->get(TBL_SAS_TFORM);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('sas_tform.*, users.Fullname, sas_mprovinsi.Provinsi')
    ->join(TBL_SAS_MPROVINSI,TBL_SAS_MPROVINSI.'.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDPROVINSI,"left")
    ->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL_SAS_TFORM.".".COL_USERNAME,"left")
    ->get_compiled_select(TBL_SAS_TFORM, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/sas/result/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary"><i class="fas fa-search"></i>&nbsp;LIHAT HASIL</a>&nbsp;';

      $rdetsum = $this->db
      ->select('AVG(sas_tformdetail.NumNR) as AvgNilai')
      ->where(COL_IDFORM, $r[COL_UNIQ])
      ->get(TBL_SAS_TFORMDETAIL)
      ->row_array();

      $rdetmin = $this->db
      ->select('sas_tformdetail.*, sas_mmapel.MapelNama')
      ->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
      ->where(TBL_SAS_TFORMDETAIL.'.'.COL_IDFORM, $r[COL_UNIQ])
      ->order_by(TBL_SAS_TFORMDETAIL.'.'.COL_NUMNR,'asc')
      ->get(TBL_SAS_TFORMDETAIL)
      ->row_array();

      $rdetmax = $this->db
      ->select('sas_tformdetail.*, sas_mmapel.MapelNama')
      ->join(TBL_SAS_MMAPEL,TBL_SAS_MMAPEL.'.'.COL_UNIQ." = ".TBL_SAS_TFORMDETAIL.".".COL_IDMAPEL,"left")
      ->where(TBL_SAS_TFORMDETAIL.'.'.COL_IDFORM, $r[COL_UNIQ])
      ->order_by(TBL_SAS_TFORMDETAIL.'.'.COL_NUMNR,'desc')
      ->get(TBL_SAS_TFORMDETAIL)
      ->row_array();

      $numNR = !empty($rdetsum)?$rdetsum['AvgNilai']:0;
      $numNilaiPerc = 0;
      $numAkreditasi = 0;
      $numDomisiliPT1 = 0;
      $numDomisiliPT2 = 0;
      $numAlumni = 0;
      $numRankSekolah = 0;
      $numRankSiswa = 0;
      $numMapelPendukung = 0;
      $numPiagam = 0;
      $numPortfolio = 0;
      $txtPeluang = '';
      $numGrade = 0;
      $numLevel = 0;

      $arrResult = array();

      if($r[COL_FORMPORTOFOLIO]=='ADA') {
        $rmatriks = $this->db
        ->where(COL_MATNAME,'NILAI_WITH_PORTOFOLIO')
        ->where(COL_MATRANGEFROM.'<=',$numNR)
        ->where(COL_MATRANGETO.'>=',$numNR)
        ->order_by(COL_MATRANGEFROM,'desc')
        ->get(TBL_SAS_MMATRIKS)
        ->row_array();

        $numNilaiPerc = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
        $arrResult[] = $numNilaiPerc;
      } else if($r[COL_FORMPORTOFOLIO]=='TIDAK') {
        $rmatriks = $this->db
        ->where(COL_MATNAME,'NILAI_NO_PORTOFOLIO')
        ->where(COL_MATRANGEFROM.'<=',$numNR)
        ->where(COL_MATRANGETO.'>=',$numNR)
        ->order_by(COL_MATRANGEFROM,'desc')
        ->get(TBL_SAS_MMATRIKS)
        ->row_array();

        $numNilaiPerc = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
        $arrResult[] = $numNilaiPerc;
      }

      $rmatriks = $this->db
      ->where(COL_MATNAME,'AKREDITASI')
      ->where(COL_MATVAL,$r[COL_FORMAKREDITAS])
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numAkreditasi = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
      $arrResult[] = $numAkreditasi;

      if($r[COL_IDPROVINSI]==$r[COL_IDPROVINSIPT1]) {
        $rmatriks = $this->db
        ->where(COL_MATNAME,'DOMISILI_PTN1')
        ->where(COL_MATVAL,'YA')
        ->get(TBL_SAS_MMATRIKS)
        ->row_array();
        $numDomisiliPT1 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
        $arrResult[] = $numDomisiliPT1;
      } else {
        $rmatriks = $this->db
        ->where(COL_MATNAME,'DOMISILI_PTN1')
        ->where(COL_MATVAL,'TIDAK')
        ->get(TBL_SAS_MMATRIKS)
        ->row_array();
        $numDomisiliPT1 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
        $arrResult[] = $numDomisiliPT1;
      }

      if($r[COL_IDPROVINSI]==$r[COL_IDPROVINSIPT2]) {
        $rmatriks = $this->db
        ->where(COL_MATNAME,'DOMISILI_PTN2')
        ->where(COL_MATVAL,'YA')
        ->get(TBL_SAS_MMATRIKS)
        ->row_array();
        $numDomisiliPT2 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
        $arrResult[] = $numDomisiliPT2;
      } else {
        $rmatriks = $this->db
        ->where(COL_MATNAME,'DOMISILI_PTN2')
        ->where(COL_MATVAL,'TIDAK')
        ->get(TBL_SAS_MMATRIKS)
        ->row_array();
        $numDomisiliPT2 = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
        $arrResult[] = $numDomisiliPT2;
      }

      $rmatriks = $this->db
      ->where(COL_MATNAME,'ALUMNI_SNBP')
      ->where(COL_MATVAL,$r[COL_FORMNUMALUMNI])
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numAlumni = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
      $arrResult[] = $numAlumni;

      $rmatriks = $this->db
      ->where(COL_MATNAME,'RANKING_SEKOLAH')
      ->where(COL_MATVAL,$r[COL_FORMRANKSEKOLAH])
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numRankSekolah = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
      $arrResult[] = $numRankSekolah;

      $rmatriks = $this->db
      ->where(COL_MATNAME,'RANK_PARALEL')
      ->where(COL_MATVAL,$r[COL_FORMRANKPARALEL])
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numRankSiswa = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
      $arrResult[] = $numRankSiswa;

      $rmatriks = $this->db
      ->where(COL_MATNAME,'MAPEL_PENDUKUNG')
      ->where(COL_MATVAL,$r[COL_FORMMAPELKESESUAIAN])
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numMapelPendukung = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
      $arrResult[] = $numMapelPendukung;

      $rmatriks = $this->db
      ->where(COL_MATNAME,'PIAGAM')
      ->where(COL_MATVAL,$r[COL_FORMPIAGAM])
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numPiagam = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
      $arrResult[] = $numPiagam;

      $rmatriks = $this->db
      ->where(COL_MATNAME,'PORTOFOLIO')
      ->where(COL_MATVAL,$r[COL_FORMPORTOFOLIO])
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numPortfolio = !empty($rmatriks)?$rmatriks[COL_MATPOINT]:0;
      $arrResult[] = $numPortfolio;

      $rmatriks = $this->db
      ->where(COL_MATNAME,'PELUANG')
      ->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
      ->where(COL_MATRANGETO.'>=',array_sum($arrResult))
      ->order_by(COL_MATRANGEFROM,'desc')
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $txtPeluang = !empty($rmatriks)?$rmatriks[COL_MATVAL]:'';

      $rmatriksGrade = $this->db
      ->where(COL_MATNAME,'ANALYSIS_GRADE')
      ->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
      ->where(COL_MATRANGETO.'>=',array_sum($arrResult))
      ->order_by(COL_MATRANGEFROM,'desc')
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numGrade = !empty($rmatriksGrade)?$rmatriksGrade[COL_MATPOINT]:0;

      $rmatriksLevel = $this->db
      ->where(COL_MATNAME,'ANALYSIS_LEVEL')
      ->where(COL_MATRANGEFROM.'<=',array_sum($arrResult))
      ->where(COL_MATRANGETO.'>=',array_sum($arrResult))
      ->order_by(COL_MATRANGEFROM,'desc')
      ->get(TBL_SAS_MMATRIKS)
      ->row_array();
      $numLevel = !empty($rmatriksLevel)?$rmatriksLevel[COL_MATPOINT]:0;

      $data[] = array(
        $htmlBtn,
        $r[COL_FULLNAME].' <br /> <small class="font-italic">'.$r[COL_USERNAME].'</small>',
        date('d-m-Y', strtotime($r[COL_CREATEDON])).' <br /> <small class="font-italic">'.date('H:i', strtotime($r[COL_CREATEDON])).'</small>',
        $r[COL_FORMASALSEKOLAH].' <br /> <small class="font-italic">'.$r[COL_PROVINSI].'</small>',
        '<strong>'.number_format(array_sum($arrResult)).'</strong>'
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function form() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $formNama = $ruser[COL_FULLNAME];
      $formUsername = $ruser[COL_USERNAME];
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $ruser_ = $this->db->where(COL_USERNAME, $this->input->post(COL_USERNAME))->get(TBL_USERS)->row_array();
        if(empty($ruser_)) {
          ShowJsonError('Data pengguna tidak valid!');
          exit();
        }

        $formUsername = $this->input->post(COL_USERNAME);
        $formNama = $ruser_[COL_FULLNAME];
      }
      $dat = array(
        COL_USERNAME => $formUsername,
        COL_FORMNAMA => $formNama,
        COL_IDPROVINSI => $this->input->post(COL_IDPROVINSI),
        COL_IDPRODI1 => $this->input->post(COL_IDPRODI1),
        COL_IDPRODI2 => $this->input->post(COL_IDPRODI2),
        COL_IDPROVINSIPT1 => $this->input->post(COL_IDPROVINSIPT1),
        COL_IDPROVINSIPT2 => $this->input->post(COL_IDPROVINSIPT2),
        COL_IDMAPEL1 => $this->input->post(COL_IDMAPEL1),
        COL_IDMAPEL2 => $this->input->post(COL_IDMAPEL2),
        COL_FORMMAPELNILAI1 => $this->input->post(COL_FORMMAPELNILAI1),
        COL_FORMMAPELNILAI2 => $this->input->post(COL_FORMMAPELNILAI2),
        COL_FORMMAPELKESESUAIAN => $this->input->post(COL_FORMMAPELKESESUAIAN),
        COL_FORMJK => $this->input->post(COL_FORMJK),
        COL_FORMASALSEKOLAH => $this->input->post(COL_FORMASALSEKOLAH),
        COL_FORMAKREDITAS => $this->input->post(COL_FORMAKREDITAS),
        COL_FORMJURUSAN => $this->input->post(COL_FORMJURUSAN),
        COL_FORMPROGRAM => $this->input->post(COL_FORMPROGRAM),
        COL_FORMPIAGAM => $this->input->post(COL_FORMPIAGAM),
        COL_FORMNUMALUMNI => $this->input->post(COL_FORMNUMALUMNI),
        COL_FORMRANKSEKOLAH => $this->input->post(COL_FORMRANKSEKOLAH),
        COL_FORMRANKPARALEL => $this->input->post(COL_FORMRANKPARALEL),
        COL_FORMPORTOFOLIO => $this->input->post(COL_FORMPORTOFOLIO),
        COL_CREATEDBY=>$ruser[COL_USERNAME],
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );
      $det = array();
      $arrIdMapel = $this->input->post('IdMapel');
      $arrNumNR = $this->input->post('NumNR');

      if(empty($arrIdMapel) || empty($arrNumNR)) {
        ShowJsonError('Data nilai tidak valid!');
        exit();
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_SAS_TFORM, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $idForm = $this->db->insert_id();
        for($i=0; $i<count($arrIdMapel); $i++) {
          $det[] = array(
            COL_IDFORM=>$idForm,
            COL_IDMAPEL=>$arrIdMapel[$i],
            COL_NUMNR=>$arrNumNR[$i],
          );
        }
        $res = $this->db->insert_batch(TBL_SAS_TFORMDETAIL, $det);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil mengirim data. Mengolah data...', array('redirect'=>current_url()));
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
      for($i=0; $i<count($arrIdMapel); $i++) {
        $det[] = array(

        );
      }
    } else {
      $data['title'] = 'SNBP ANALYSIS SYSTEM (SAS)';
      $this->template->load('adminlte-top', 'sas/form', $data);
    }
  }

  public function result($id, $opt='') {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->select('sas_tform.*,
              sas_mprovinsi.Provinsi,
              ProvPT1.Provinsi as ProvinsiPTN1,
              ProvPT2.Provinsi as ProvinsiPTN2,
              Prodi1.ProdiKode as ProdiKode1,
              Prodi1.ProdiNama as ProdiNama1,
              Prodi1.ProdiPT as ProdiPT1,
              Prodi1.NumNilai as ProdiNilai1,
              Prodi2.ProdiKode as ProdiKode2,
              Prodi2.ProdiNama as ProdiNama2,
              Prodi2.ProdiPT as ProdiPT2,
              Prodi2.NumNilai as ProdiNilai2,
              Mapel1.MapelNama as Mapel1,
              Mapel2.MapelNama as Mapel2')
    ->join(TBL_SAS_MPROVINSI,TBL_SAS_MPROVINSI.'.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDPROVINSI,"left")
    ->join(TBL_SAS_MPROVINSI.' ProvPT1','ProvPT1.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDPROVINSIPT1,"left")
    ->join(TBL_SAS_MPROVINSI.' ProvPT2','ProvPT2.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDPROVINSIPT2,"left")
    ->join(TBL_SAS_MPRODI.' Prodi1','Prodi1.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDPRODI1,"left")
    ->join(TBL_SAS_MPRODI.' Prodi2','Prodi2.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDPRODI2,"left")
    ->join(TBL_SAS_MMAPEL.' Mapel1','Mapel1.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDMAPEL1,"left")
    ->join(TBL_SAS_MMAPEL.' Mapel2','Mapel2.'.COL_UNIQ." = ".TBL_SAS_TFORM.".".COL_IDMAPEL2,"left")
    ->where(TBL_SAS_TFORM.'.'.COL_UNIQ, $id)
    ->get(TBL_SAS_TFORM)->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if($ruser[COL_ROLEID]!=ROLEADMIN && $ruser[COL_USERNAME]!=$rdata[COL_USERNAME]) {
      show_error('Anda tidak memiliki hak akses!');
      exit();
    }

    $data['title'] = 'SNBP ANALYSIS SYSTEM (SAS)';
    $data['data'] = $rdata;

    if($opt=='print') {
      $this->load->library('Mypdf');
      $mpdf = new Mypdf('','A4',0,'',15,15,36,16);

      $html = $this->load->view('site/sas/result-print', $data, TRUE);
      //echo $html;
      //exit();
      $htmlTitle = strtoupper($this->setting_web_name);
      $htmlTitleSub = 'SNBP ANALYSIS SYSTEM (SAS)'; //'www.daksastudio.id<br />'.$this->setting_org_mail.' / '.$this->setting_org_phone.' / @id.daksastudio<br />'.$this->setting_org_address;
      $htmlLogo = base_url().$this->setting_web_logo;
      $htmlHeader = @"
      <table style=\"border: none !important\">
        <tr>
          <td style=\"border: none !important; padding: 0 !important; font-size: 10pt; font-weight: bold; vertical-align: top; text-align: center\"><b>$htmlTitle</b><br /><small style=\"font-weight: normal\">$htmlTitleSub</small></td>
        </tr>
      </table>
      <hr />
      ";

      $mpdf->pdf->SetTitle($htmlTitle);
      $mpdf->pdf->SetHTMLHeader($htmlHeader);
      $mpdf->pdf->SetWatermarkImage(MY_IMAGEPATH.'logo-full-square.png');
      $mpdf->pdf->showWatermarkImage = true;
      $mpdf->pdf->SetFooter('Dicetak melalui laman web <strong>'.$this->setting_web_name.'</strong>');
      $mpdf->pdf->WriteHTML($html);
      $mpdf->pdf->use_kwt = true;
      $mpdf->pdf->Output($this->setting_web_name.' - '.$htmlTitle.'.pdf', 'I');
    } else {
      $this->template->load('adminlte-top', 'sas/result', $data);
    }
  }

  public function browse_prodi() {
    $res = array();
    $rprodi = $this->db
    ->order_by(COL_PRODIPT)
    ->order_by(COL_PRODITIPE)
    ->order_by(COL_NUMNILAI)
    ->get(TBL_SAS_MPRODI)
    ->result_array();

    $data['res'] = $rprodi;
    $this->load->view('site/sas/browse-prodi', $data);
  }

  public function load_form_nilai() {
    $jurusan = $this->input->post('Jurusan');
    $rmapel = $this->db->where(COL_MAPELKATEGORI, $jurusan)->get(TBL_SAS_MMAPEL)->result_array();
    $html = '';
    foreach($rmapel as $r) {
      $html .= @'
      <div class="form-group mb-3">
        <div class="row">
          <label class="control-label col-lg-6 col-12">'.$r[COL_MAPELNAMA].'</label>
          <div class="col-lg-4 col-12">
            <input type="hidden" name="IdMapel[]" value="'.$r[COL_UNIQ].'" required />
            <input type="number" name="NumNR[]" class="form-control" placeholder="Nilai Rata-Rata" required />
          </div>
        </div>
      </div>
      ';
    }
    echo $html;
  }
}
